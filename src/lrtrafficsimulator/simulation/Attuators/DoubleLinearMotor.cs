﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Attuators
{
    public class DoubleLinearMotor : Attuator<Float2>
    {
        private Float2 dir;
        private float L;

        public DoubleLinearMotor(string name, Float2 initialValue, Float2 pos, Float2 dir, float wheelsDistance)
            : base(name, initialValue, pos)
        {
            this.dir = dir;
            this.L = wheelsDistance;
        }

        //moves the vehicle up to reach certain position and direction
        public override void commitAttuatorChanges(IEnvironment env, ref VehicleState rs, float seconds)
        {
            float trVel = (value.X + value.Y) * 0.5f;
            float angVel = (value.Y - value.X) / L;

            Float2 trDir = new Float2(dir.Degrees + rs.Direction.Degrees);
            rs.Position += trDir * trVel * seconds;
            rs.Direction = new Float2(rs.Direction.Degrees + angVel * seconds / (float)Math.PI * 180f);
        }
    }
}
