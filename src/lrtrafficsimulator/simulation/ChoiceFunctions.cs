﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation
{
    public static class ChoiceFunctions
    //This class contains some choice functions used to select the next street to travel. 
    //The traffic conditions are different depending on the choice function used.
    {
        public static ChoiceFunction[] AllFunctions
        {
            get
            {
                return new ChoiceFunction[] { AlwaysDefault, RandomChoice, BestChoicePERC20, BestChoicePERC50, DeltaTraffic00020, DeltaTraffic00015, DeltaTraffic00010, DeltaTraffic00005, DotDelta00010, DotDelta00005, DotDelta00002, ClosestAlternative };
            }

        }

        public static int AlwaysDefault(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return defaultChoiceIndex;
        }

        public static int RandomChoice(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return new Random().Next(0, pi.Length);
        }

        public static int BestChoicePERC20(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return BestChoiceTHR(defaultChoiceIndex, curAdj, destination, 0.20f, pi);
        }

        public static int BestChoicePERC50(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return BestChoiceTHR(defaultChoiceIndex, curAdj, destination, 0.50f, pi);
        }

        public static int DeltaTraffic00020(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return DeltaTraffic(defaultChoiceIndex, curAdj, destination, 0.002f, pi);
        }

        public static int DeltaTraffic00010(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return DeltaTraffic(defaultChoiceIndex, curAdj, destination, 0.001f, pi);
        }

        public static int DeltaTraffic00015(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return DeltaTraffic(defaultChoiceIndex, curAdj, destination, 0.0015f, pi);
        }

        public static int DeltaTraffic00005(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return DeltaTraffic(defaultChoiceIndex, curAdj, destination, 0.0005f, pi);
        }

        public static int DotDelta00005(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return DotDelta(defaultChoiceIndex, curAdj, 0.0005f, destination, curPos, adjacentNodes, pi);
        }

        public static int DotDelta00002(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return DotDelta(defaultChoiceIndex, curAdj, 0.0002f, destination, curPos, adjacentNodes, pi);
        }

        public static int DotDelta00010(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            return DotDelta(defaultChoiceIndex, curAdj, 0.001f, destination, curPos, adjacentNodes, pi);
        }

        private const float WRONG_WAY = -0.6f;
        private static int DotDelta(int defaultChoiceIndex, int curAdj, float delta, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            float lowestTraffic = pi[defaultChoiceIndex];
           
            int bestAdj = defaultChoiceIndex;
            float dot = -1.0f;

            for (int i = 0; i < pi.Length; i++)
            {
                if (i == defaultChoiceIndex || i == curAdj) continue;

                float curDot = Float2.DotProduct((adjacentNodes[i] - curPos).GetNormal(),(destination - curPos).GetNormal());
                if (pi[i] < pi[defaultChoiceIndex] && dot < curDot && curDot > WRONG_WAY)
                {
                    lowestTraffic = pi[i];
                    bestAdj = i;
                    dot = curDot;
                }
            }

            if (pi[defaultChoiceIndex] - lowestTraffic > delta) return bestAdj;

            return defaultChoiceIndex;
        }


        private const float CLOSE_WAY = -0.1f;
        public static int ClosestAlternative(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi)
        {
            int bestAdj = defaultChoiceIndex;
            float dot = -1.0f;

            for (int i = 0; i < pi.Length; i++)
            {
                if (i == defaultChoiceIndex || i == curAdj) continue;

                float curDot = Float2.DotProduct((adjacentNodes[i] - curPos).GetNormal(), (destination - curPos).GetNormal());
                if (dot < curDot && curDot > CLOSE_WAY)
                {
                    bestAdj = i;
                    dot = curDot;
                }
            }

            if (pi[defaultChoiceIndex] > pi[bestAdj]) 
                return bestAdj;
            else
                return defaultChoiceIndex;
        }

        private static int DeltaTraffic(int defaultChoiceIndex, int curAdj, Float2 destination, float delta, params float[] pi)
        {
            float lowestTraffic = pi[defaultChoiceIndex];
            int bestAdj = defaultChoiceIndex;

            for (int i = 0; i < pi.Length; i++)
            {
                if (i == defaultChoiceIndex || i == curAdj) continue;

                if (pi[i] < lowestTraffic)
                {
                    lowestTraffic = pi[i];
                    bestAdj = i;
                }
            }

            if (pi[defaultChoiceIndex] - lowestTraffic > delta) return bestAdj;

            return defaultChoiceIndex;
        }

        private static int BestChoiceTHR(int defaultChoiceIndex, int curAdj, Float2 destination, float thr, params float[] pi)
        {
            float lowestTraffic = pi[defaultChoiceIndex];
            int bestAdj = defaultChoiceIndex;

            for (int i = 0; i < pi.Length; i++)
            {
                if (i == defaultChoiceIndex || i == curAdj) continue;

                if (pi[i] < lowestTraffic)
                {
                    lowestTraffic = pi[i];
                    bestAdj = i;
                }
            }

            if ((1f - lowestTraffic) / pi[defaultChoiceIndex] > thr)
            {
                return bestAdj;
            }
            else
            {
                return defaultChoiceIndex;
            }
        }

    }

    public delegate int ChoiceFunction(int defaultChoiceIndex, int curAdj, Float2 destination, Float2 curPos, Float2[] adjacentNodes, params float[] pi);
}
