﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.GUI
{
    public interface ISimDrawer
    {
        void DrawSimulationFrame(Graphics g);

        void SetView(string name);
    }
}
