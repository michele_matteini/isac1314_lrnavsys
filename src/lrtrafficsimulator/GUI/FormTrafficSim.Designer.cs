﻿namespace LRTrafficSimulator.GUI
{
    partial class FormTrafficSim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlInspectionTools = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnResetSimResults = new System.Windows.Forms.Button();
            this.btnCopySimTimes = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.lstSimEndTimes = new System.Windows.Forms.ListBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.nComplPerc = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.btnCopyResults = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.lstVehicleTimes = new System.Windows.Forms.ListBox();
            this.lblArrivedVehicles = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblSimEndTime = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxSkipFrame = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxVehicles = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxVisual = new System.Windows.Forms.ComboBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblTime = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboBoxCFuncs = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxDFuncs = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxGPSAI = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.nRepeatTimes = new System.Windows.Forms.NumericUpDown();
            this.checkBoxRepeat = new System.Windows.Forms.CheckBox();
            this.lblSpeedMul = new System.Windows.Forms.Label();
            this.checkBoxMapFromFile = new System.Windows.Forms.CheckBox();
            this.checkBoxSeed = new System.Windows.Forms.CheckBox();
            this.txtSeed = new System.Windows.Forms.TextBox();
            this.slowButton = new System.Windows.Forms.Button();
            this.newSimButton = new System.Windows.Forms.Button();
            this.fastButton = new System.Windows.Forms.Button();
            this.startPauseButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nTransmRange = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nDestinations = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.nIntersections = new System.Windows.Forms.NumericUpDown();
            this.nCars = new System.Windows.Forms.NumericUpDown();
            this.lblNSorg = new System.Windows.Forms.Label();
            this.lblNRobots = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.simulationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startSimulatioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.pnlInspectionTools.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nComplPerc)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nRepeatTimes)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nTransmRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nDestinations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nIntersections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nCars)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 571);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(844, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(42, 17);
            this.lblStatus.Text = "Ready.";
            // 
            // pnlInspectionTools
            // 
            this.pnlInspectionTools.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlInspectionTools.Controls.Add(this.groupBox3);
            this.pnlInspectionTools.Controls.Add(this.groupBox5);
            this.pnlInspectionTools.Location = new System.Drawing.Point(593, 27);
            this.pnlInspectionTools.Name = "pnlInspectionTools";
            this.pnlInspectionTools.Size = new System.Drawing.Size(239, 541);
            this.pnlInspectionTools.TabIndex = 8;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.btnResetSimResults);
            this.groupBox3.Controls.Add(this.btnCopySimTimes);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.lstSimEndTimes);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.nComplPerc);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.btnCopyResults);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.lstVehicleTimes);
            this.groupBox3.Controls.Add(this.lblArrivedVehicles);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.lblSimEndTime);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(3, 127);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(233, 411);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Simulation Results";
            // 
            // btnResetSimResults
            // 
            this.btnResetSimResults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetSimResults.Location = new System.Drawing.Point(96, 277);
            this.btnResetSimResults.Name = "btnResetSimResults";
            this.btnResetSimResults.Size = new System.Drawing.Size(64, 20);
            this.btnResetSimResults.TabIndex = 14;
            this.btnResetSimResults.Text = "Reset";
            this.btnResetSimResults.UseVisualStyleBackColor = true;
            this.btnResetSimResults.Click += new System.EventHandler(this.btnResetSimResults_Click);
            // 
            // btnCopySimTimes
            // 
            this.btnCopySimTimes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopySimTimes.Location = new System.Drawing.Point(163, 277);
            this.btnCopySimTimes.Name = "btnCopySimTimes";
            this.btnCopySimTimes.Size = new System.Drawing.Size(64, 20);
            this.btnCopySimTimes.TabIndex = 13;
            this.btnCopySimTimes.Text = "Copy";
            this.btnCopySimTimes.UseVisualStyleBackColor = true;
            this.btnCopySimTimes.Click += new System.EventHandler(this.btnCopySimTimes_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 281);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Simulation Times:";
            // 
            // lstSimEndTimes
            // 
            this.lstSimEndTimes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstSimEndTimes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstSimEndTimes.ForeColor = System.Drawing.Color.Teal;
            this.lstSimEndTimes.FormattingEnabled = true;
            this.lstSimEndTimes.Location = new System.Drawing.Point(6, 303);
            this.lstSimEndTimes.Name = "lstSimEndTimes";
            this.lstSimEndTimes.ScrollAlwaysVisible = true;
            this.lstSimEndTimes.Size = new System.Drawing.Size(221, 95);
            this.lstSimEndTimes.TabIndex = 11;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.DimGray;
            this.label16.Location = new System.Drawing.Point(173, 29);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "%";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.DimGray;
            this.label15.Location = new System.Drawing.Point(8, 50);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(116, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "of vehicles has arrived.";
            // 
            // nComplPerc
            // 
            this.nComplPerc.Location = new System.Drawing.Point(127, 27);
            this.nComplPerc.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nComplPerc.Name = "nComplPerc";
            this.nComplPerc.Size = new System.Drawing.Size(40, 20);
            this.nComplPerc.TabIndex = 8;
            this.nComplPerc.Value = new decimal(new int[] {
            95,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.DimGray;
            this.label14.Location = new System.Drawing.Point(8, 29);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(113, 13);
            this.label14.TabIndex = 7;
            this.label14.Text = "Simulation ends when:";
            // 
            // btnCopyResults
            // 
            this.btnCopyResults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyResults.Location = new System.Drawing.Point(121, 140);
            this.btnCopyResults.Name = "btnCopyResults";
            this.btnCopyResults.Size = new System.Drawing.Size(106, 20);
            this.btnCopyResults.TabIndex = 6;
            this.btnCopyResults.Text = "Copy to clipboard";
            this.btnCopyResults.UseVisualStyleBackColor = true;
            this.btnCopyResults.Click += new System.EventHandler(this.btnCopyResults_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 144);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Vehicle Times:";
            // 
            // lstVehicleTimes
            // 
            this.lstVehicleTimes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstVehicleTimes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstVehicleTimes.ForeColor = System.Drawing.Color.Teal;
            this.lstVehicleTimes.FormattingEnabled = true;
            this.lstVehicleTimes.Location = new System.Drawing.Point(9, 163);
            this.lstVehicleTimes.Name = "lstVehicleTimes";
            this.lstVehicleTimes.ScrollAlwaysVisible = true;
            this.lstVehicleTimes.Size = new System.Drawing.Size(218, 108);
            this.lstVehicleTimes.TabIndex = 4;
            // 
            // lblArrivedVehicles
            // 
            this.lblArrivedVehicles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblArrivedVehicles.BackColor = System.Drawing.Color.White;
            this.lblArrivedVehicles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblArrivedVehicles.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrivedVehicles.ForeColor = System.Drawing.Color.Teal;
            this.lblArrivedVehicles.Location = new System.Drawing.Point(96, 100);
            this.lblArrivedVehicles.Name = "lblArrivedVehicles";
            this.lblArrivedVehicles.Size = new System.Drawing.Size(131, 31);
            this.lblArrivedVehicles.TabIndex = 3;
            this.lblArrivedVehicles.Text = "N/A";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Arrived Vehicles:";
            // 
            // lblSimEndTime
            // 
            this.lblSimEndTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSimEndTime.BackColor = System.Drawing.Color.White;
            this.lblSimEndTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSimEndTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSimEndTime.ForeColor = System.Drawing.Color.Teal;
            this.lblSimEndTime.Location = new System.Drawing.Point(96, 77);
            this.lblSimEndTime.Name = "lblSimEndTime";
            this.lblSimEndTime.Size = new System.Drawing.Size(131, 20);
            this.lblSimEndTime.TabIndex = 1;
            this.lblSimEndTime.Text = "N/A";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Simulation End:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Skip Frame:";
            // 
            // comboBoxSkipFrame
            // 
            this.comboBoxSkipFrame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSkipFrame.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSkipFrame.FormattingEnabled = true;
            this.comboBoxSkipFrame.Items.AddRange(new object[] {
            "<Never>",
            "1 over 2",
            "1 over 3",
            "1 over 4",
            "1 over 5",
            "1 over 6",
            "1 over 7",
            "1 over 8",
            "1 over 9",
            "1 over 10",
            "1 over 11",
            "1 over 12"});
            this.comboBoxSkipFrame.Location = new System.Drawing.Point(86, 81);
            this.comboBoxSkipFrame.Name = "comboBoxSkipFrame";
            this.comboBoxSkipFrame.Size = new System.Drawing.Size(141, 21);
            this.comboBoxSkipFrame.TabIndex = 1;
            this.comboBoxSkipFrame.SelectedIndexChanged += new System.EventHandler(this.comboBoxSkipFrame_SelectedIndexChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.comboBoxVehicles);
            this.groupBox5.Controls.Add(this.comboBoxSkipFrame);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.comboBoxVisual);
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(233, 118);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Visualization";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Path trace for:";
            // 
            // comboBoxVehicles
            // 
            this.comboBoxVehicles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxVehicles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxVehicles.FormattingEnabled = true;
            this.comboBoxVehicles.Location = new System.Drawing.Point(86, 54);
            this.comboBoxVehicles.Name = "comboBoxVehicles";
            this.comboBoxVehicles.Size = new System.Drawing.Size(141, 21);
            this.comboBoxVehicles.TabIndex = 0;
            this.comboBoxVehicles.SelectedIndexChanged += new System.EventHandler(this.comboBoxVehicles_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "View:";
            // 
            // comboBoxVisual
            // 
            this.comboBoxVisual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxVisual.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxVisual.FormattingEnabled = true;
            this.comboBoxVisual.Items.AddRange(new object[] {
            "Map and vehicles",
            "Traffic gradient"});
            this.comboBoxVisual.Location = new System.Drawing.Point(86, 27);
            this.comboBoxVisual.Name = "comboBoxVisual";
            this.comboBoxVisual.Size = new System.Drawing.Size(141, 21);
            this.comboBoxVisual.TabIndex = 0;
            this.comboBoxVisual.SelectedIndexChanged += new System.EventHandler(this.comboBoxVisual_SelectedIndexChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 27);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lblTime);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox4);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(620, 541);
            this.splitContainer1.SplitterDistance = 209;
            this.splitContainer1.TabIndex = 6;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblTime.Location = new System.Drawing.Point(8, 503);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(108, 25);
            this.lblTime.TabIndex = 0;
            this.lblTime.Text = "Time:N/A";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.comboBoxCFuncs);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.comboBoxDFuncs);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.comboBoxGPSAI);
            this.groupBox4.Location = new System.Drawing.Point(7, 378);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(199, 122);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Simulation Params";
            // 
            // comboBoxCFuncs
            // 
            this.comboBoxCFuncs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxCFuncs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCFuncs.FormattingEnabled = true;
            this.comboBoxCFuncs.Location = new System.Drawing.Point(85, 73);
            this.comboBoxCFuncs.Name = "comboBoxCFuncs";
            this.comboBoxCFuncs.Size = new System.Drawing.Size(108, 21);
            this.comboBoxCFuncs.TabIndex = 5;
            this.comboBoxCFuncs.SelectedIndexChanged += new System.EventHandler(this.comboBoxCFuncs_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Choice func:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Distance func:";
            // 
            // comboBoxDFuncs
            // 
            this.comboBoxDFuncs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDFuncs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDFuncs.FormattingEnabled = true;
            this.comboBoxDFuncs.Location = new System.Drawing.Point(85, 46);
            this.comboBoxDFuncs.Name = "comboBoxDFuncs";
            this.comboBoxDFuncs.Size = new System.Drawing.Size(108, 21);
            this.comboBoxDFuncs.TabIndex = 2;
            this.comboBoxDFuncs.SelectedIndexChanged += new System.EventHandler(this.comboBoxDFuncs_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "GPS AI:";
            // 
            // comboBoxGPSAI
            // 
            this.comboBoxGPSAI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxGPSAI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGPSAI.FormattingEnabled = true;
            this.comboBoxGPSAI.Location = new System.Drawing.Point(85, 19);
            this.comboBoxGPSAI.Name = "comboBoxGPSAI";
            this.comboBoxGPSAI.Size = new System.Drawing.Size(108, 21);
            this.comboBoxGPSAI.TabIndex = 0;
            this.comboBoxGPSAI.SelectedIndexChanged += new System.EventHandler(this.comboBoxControllers_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.nRepeatTimes);
            this.groupBox2.Controls.Add(this.checkBoxRepeat);
            this.groupBox2.Controls.Add(this.lblSpeedMul);
            this.groupBox2.Controls.Add(this.checkBoxMapFromFile);
            this.groupBox2.Controls.Add(this.checkBoxSeed);
            this.groupBox2.Controls.Add(this.txtSeed);
            this.groupBox2.Controls.Add(this.slowButton);
            this.groupBox2.Controls.Add(this.newSimButton);
            this.groupBox2.Controls.Add(this.fastButton);
            this.groupBox2.Controls.Add(this.startPauseButton);
            this.groupBox2.Location = new System.Drawing.Point(7, 154);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(199, 218);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Commands";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(133, 189);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "times";
            // 
            // nRepeatTimes
            // 
            this.nRepeatTimes.Location = new System.Drawing.Point(79, 187);
            this.nRepeatTimes.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nRepeatTimes.Name = "nRepeatTimes";
            this.nRepeatTimes.Size = new System.Drawing.Size(49, 20);
            this.nRepeatTimes.TabIndex = 12;
            this.nRepeatTimes.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // checkBoxRepeat
            // 
            this.checkBoxRepeat.AutoSize = true;
            this.checkBoxRepeat.Location = new System.Drawing.Point(9, 188);
            this.checkBoxRepeat.Name = "checkBoxRepeat";
            this.checkBoxRepeat.Size = new System.Drawing.Size(64, 17);
            this.checkBoxRepeat.TabIndex = 11;
            this.checkBoxRepeat.Text = "Repeat:";
            this.checkBoxRepeat.UseVisualStyleBackColor = true;
            // 
            // lblSpeedMul
            // 
            this.lblSpeedMul.AutoSize = true;
            this.lblSpeedMul.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpeedMul.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.lblSpeedMul.Location = new System.Drawing.Point(132, 69);
            this.lblSpeedMul.Name = "lblSpeedMul";
            this.lblSpeedMul.Size = new System.Drawing.Size(27, 20);
            this.lblSpeedMul.TabIndex = 10;
            this.lblSpeedMul.Text = "x1";
            // 
            // checkBoxMapFromFile
            // 
            this.checkBoxMapFromFile.AutoSize = true;
            this.checkBoxMapFromFile.Location = new System.Drawing.Point(9, 165);
            this.checkBoxMapFromFile.Name = "checkBoxMapFromFile";
            this.checkBoxMapFromFile.Size = new System.Drawing.Size(86, 17);
            this.checkBoxMapFromFile.TabIndex = 9;
            this.checkBoxMapFromFile.Text = "Map from file";
            this.checkBoxMapFromFile.UseVisualStyleBackColor = true;
            // 
            // checkBoxSeed
            // 
            this.checkBoxSeed.AutoSize = true;
            this.checkBoxSeed.Location = new System.Drawing.Point(9, 141);
            this.checkBoxSeed.Name = "checkBoxSeed";
            this.checkBoxSeed.Size = new System.Drawing.Size(54, 17);
            this.checkBoxSeed.TabIndex = 8;
            this.checkBoxSeed.Text = "Seed:";
            this.checkBoxSeed.UseVisualStyleBackColor = true;
            // 
            // txtSeed
            // 
            this.txtSeed.Location = new System.Drawing.Point(69, 139);
            this.txtSeed.Name = "txtSeed";
            this.txtSeed.Size = new System.Drawing.Size(124, 20);
            this.txtSeed.TabIndex = 7;
            // 
            // slowButton
            // 
            this.slowButton.Enabled = false;
            this.slowButton.Location = new System.Drawing.Point(6, 81);
            this.slowButton.Name = "slowButton";
            this.slowButton.Size = new System.Drawing.Size(113, 23);
            this.slowButton.TabIndex = 5;
            this.slowButton.Text = "Decrease speed";
            this.slowButton.UseVisualStyleBackColor = true;
            this.slowButton.Click += new System.EventHandler(this.slowButton_Click);
            // 
            // newSimButton
            // 
            this.newSimButton.Location = new System.Drawing.Point(5, 110);
            this.newSimButton.Name = "newSimButton";
            this.newSimButton.Size = new System.Drawing.Size(114, 23);
            this.newSimButton.TabIndex = 4;
            this.newSimButton.Text = "New simulation";
            this.newSimButton.UseVisualStyleBackColor = true;
            this.newSimButton.Click += new System.EventHandler(this.newSimButton_Click);
            // 
            // fastButton
            // 
            this.fastButton.Enabled = false;
            this.fastButton.Location = new System.Drawing.Point(6, 52);
            this.fastButton.Name = "fastButton";
            this.fastButton.Size = new System.Drawing.Size(113, 23);
            this.fastButton.TabIndex = 1;
            this.fastButton.Text = "Increase speed";
            this.fastButton.UseVisualStyleBackColor = true;
            this.fastButton.Click += new System.EventHandler(this.fastButton_Click);
            // 
            // startPauseButton
            // 
            this.startPauseButton.Enabled = false;
            this.startPauseButton.Location = new System.Drawing.Point(6, 23);
            this.startPauseButton.Name = "startPauseButton";
            this.startPauseButton.Size = new System.Drawing.Size(113, 23);
            this.startPauseButton.TabIndex = 0;
            this.startPauseButton.Text = "Start";
            this.startPauseButton.UseVisualStyleBackColor = true;
            this.startPauseButton.Click += new System.EventHandler(this.startPauseButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.nTransmRange);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.nDestinations);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.nIntersections);
            this.groupBox1.Controls.Add(this.nCars);
            this.groupBox1.Controls.Add(this.lblNSorg);
            this.groupBox1.Controls.Add(this.lblNRobots);
            this.groupBox1.Location = new System.Drawing.Point(7, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(199, 145);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Simulation parameters";
            // 
            // nTransmRange
            // 
            this.nTransmRange.Location = new System.Drawing.Point(120, 103);
            this.nTransmRange.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nTransmRange.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nTransmRange.Name = "nTransmRange";
            this.nTransmRange.Size = new System.Drawing.Size(49, 20);
            this.nTransmRange.TabIndex = 15;
            this.nTransmRange.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Tansmission Range";
            // 
            // nDestinations
            // 
            this.nDestinations.Location = new System.Drawing.Point(120, 77);
            this.nDestinations.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nDestinations.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nDestinations.Name = "nDestinations";
            this.nDestinations.Size = new System.Drawing.Size(49, 20);
            this.nDestinations.TabIndex = 13;
            this.nDestinations.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Destination Number";
            // 
            // nIntersections
            // 
            this.nIntersections.Location = new System.Drawing.Point(120, 51);
            this.nIntersections.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nIntersections.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nIntersections.Name = "nIntersections";
            this.nIntersections.Size = new System.Drawing.Size(49, 20);
            this.nIntersections.TabIndex = 11;
            this.nIntersections.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // nCars
            // 
            this.nCars.Location = new System.Drawing.Point(120, 25);
            this.nCars.Maximum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.nCars.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nCars.Name = "nCars";
            this.nCars.Size = new System.Drawing.Size(49, 20);
            this.nCars.TabIndex = 8;
            this.nCars.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // lblNSorg
            // 
            this.lblNSorg.AutoSize = true;
            this.lblNSorg.Location = new System.Drawing.Point(6, 53);
            this.lblNSorg.Name = "lblNSorg";
            this.lblNSorg.Size = new System.Drawing.Size(102, 13);
            this.lblNSorg.TabIndex = 6;
            this.lblNSorg.Text = "Intersection Number";
            // 
            // lblNRobots
            // 
            this.lblNRobots.AutoSize = true;
            this.lblNRobots.Location = new System.Drawing.Point(6, 27);
            this.lblNRobots.Name = "lblNRobots";
            this.lblNRobots.Size = new System.Drawing.Size(63, 13);
            this.lblNRobots.TabIndex = 4;
            this.lblNRobots.Text = "Car Number";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simulationToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(844, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // simulationToolStripMenuItem
            // 
            this.simulationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startSimulatioToolStripMenuItem,
            this.stopToolStripMenuItem});
            this.simulationToolStripMenuItem.Name = "simulationToolStripMenuItem";
            this.simulationToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.simulationToolStripMenuItem.Text = "Simulation";
            // 
            // startSimulatioToolStripMenuItem
            // 
            this.startSimulatioToolStripMenuItem.Enabled = false;
            this.startSimulatioToolStripMenuItem.Name = "startSimulatioToolStripMenuItem";
            this.startSimulatioToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.startSimulatioToolStripMenuItem.Text = "Start simulation";
            this.startSimulatioToolStripMenuItem.Click += new System.EventHandler(this.startSimulatioToolStripMenuItem_Click);
            // 
            // stopToolStripMenuItem
            // 
            this.stopToolStripMenuItem.Enabled = false;
            this.stopToolStripMenuItem.Name = "stopToolStripMenuItem";
            this.stopToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.stopToolStripMenuItem.Text = "Stop";
            this.stopToolStripMenuItem.Click += new System.EventHandler(this.stopToolStripMenuItem_Click);
            // 
            // FormTrafficSim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 593);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pnlInspectionTools);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "FormTrafficSim";
            this.Text = "Local Rules Navigation System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSimulator_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.pnlInspectionTools.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nComplPerc)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nRepeatTimes)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nTransmRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nDestinations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nIntersections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nCars)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.Panel pnlInspectionTools;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button slowButton;
        private System.Windows.Forms.Button newSimButton;
        private System.Windows.Forms.Button fastButton;
        private System.Windows.Forms.Button startPauseButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nIntersections;
        private System.Windows.Forms.NumericUpDown nCars;
        private System.Windows.Forms.Label lblNSorg;
        private System.Windows.Forms.Label lblNRobots;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem simulationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startSimulatioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox comboBoxVisual;
        private System.Windows.Forms.ComboBox comboBoxGPSAI;
        private System.Windows.Forms.ComboBox comboBoxVehicles;
        private System.Windows.Forms.TextBox txtSeed;
        private System.Windows.Forms.CheckBox checkBoxSeed;
        private System.Windows.Forms.NumericUpDown nDestinations;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxCFuncs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxDFuncs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxMapFromFile;
        private System.Windows.Forms.NumericUpDown nTransmRange;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxSkipFrame;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnCopyResults;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ListBox lstVehicleTimes;
        private System.Windows.Forms.Label lblArrivedVehicles;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblSimEndTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown nComplPerc;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblSpeedMul;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nRepeatTimes;
        private System.Windows.Forms.CheckBox checkBoxRepeat;
        private System.Windows.Forms.Button btnCopySimTimes;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListBox lstSimEndTimes;
        private System.Windows.Forms.Button btnResetSimResults;
    }
}