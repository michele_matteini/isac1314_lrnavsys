﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation
{  
    //This class makes the simulation time global
    public class GlobalTime
    {
        private float time;
        private float stepTime;
        private int framesComputed;
        private float lastFPS;
        private long lastFPSUpdate;
        private long lastFrameTime;
        private ReaderWriterLockSlim rwlock;

        public GlobalTime()
        {
            Reset();
        }

        public void Reset()
        {
            time = 0f;
            framesComputed = 0;
            lastFPS = 0;
            lastFPSUpdate = Environment.TickCount;
            stepTime = SimSettings.DEFAULT_STEP_TIME;
            lastFrameTime = Environment.TickCount;
            rwlock = new ReaderWriterLockSlim();
        }

        //Gets the fps
        public float FPS
        {
            get
            {
                rwlock.EnterReadLock();
                float curLastFPS = lastFPS;
                rwlock.ExitReadLock();
                return curLastFPS;
            }
        }

        //Gets the time in seconds from the beginning of the simulation
        public float SimTimeSeconds
        {
            get
            {
                rwlock.EnterReadLock();
                float curTime = time;
                rwlock.ExitReadLock();
                return curTime;
            }
        }

        //Gets the step time in seconds
        public float StepTimeSeconds
        {
            get
            {
                rwlock.EnterReadLock();
                float curStepTime = stepTime;
                rwlock.ExitReadLock();
                return curStepTime;
            }
            set
            {
                rwlock.EnterWriteLock();
                stepTime = value;
                rwlock.ExitWriteLock();
            }
        }

        //Updates the simulation time and the fps because of the execution of a single step
        public void NextSimFrame()
        {
            rwlock.EnterWriteLock();
            time += stepTime;
            long curMillisec = Environment.TickCount;


            //Update FPS
            framesComputed++;
            long dtMillisec = curMillisec - this.lastFPSUpdate;
            float curFPS = this.framesComputed * 1000.0f / (float)dtMillisec;
            if (dtMillisec >= SimSettings.FPS_UPDATE_TIME_MS)
            {
                lastFPS = curFPS;
                this.framesComputed = 0;
                this.lastFPSUpdate = curMillisec;
            }

            //Limit Frame Rate
            int remainingTime = (int)(1000.0f / (float)SimSettings.MAX_FPS) - (int)(curMillisec - lastFrameTime);
            if (remainingTime > 0) Thread.Sleep(remainingTime);
            lastFrameTime = curMillisec;
            rwlock.ExitWriteLock();
        }    

        //Returns the time in minutes and seconds from the beginning of the simulation
        public override string ToString()
        {
            rwlock.EnterReadLock();
            int seconds = (int)time;
            rwlock.ExitReadLock();
            int minutes = seconds / 60;
            seconds = seconds % 60;
            return "Time: 0" + minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
        }

        
    }
 

}
