﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.BaseTypes
{
    public interface IBoundingSurface
    {
        float DistanceFrom(Float2 samplePos, Float2 sampleDir, Float2 shapePos, Float2 shapeDir);

        float DistanceFrom(Float2 samplePos, Float2 shapePos, Float2 shapeDir);

        void DrawShape(Float2 pos, Float2 dir, Graphics g, Color3f color);
    }
}
