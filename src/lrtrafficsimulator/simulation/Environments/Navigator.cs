﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Environments
{
    public partial class CityMap
    {

        public class Navigator
        {
            private CityMap map;

            public Navigator(CityMap map)
            {
                this.map = map;
            }

            /// <summary>
            /// Compute a good path to reach the destNode from the initial position.
            /// </summary>
            public Float2[] ComputeGPSPath(int prevNode, int nextNode, int destNode)
            {
                //destination vehicle path
                Float2 destNodePos = map.nodes[destNode];
                if (destNode == nextNode) return new Float2[] { destNodePos };//se la destinazione coincide col nodo verso cui il veicolo si sta dirigendo non è necessario calcolare il percorso

                List<PathNode> pathTree = new List<PathNode>();
                List<PathNode> pathBranch = new List<PathNode>();
                List<float> adjQualities = new List<float>();

                //initialize algorithm
                PathNode startNode = new PathNode();
                startNode.lastNodeIndex = nextNode;
                startNode.pathLength = 0f;
                startNode.crossedNodes = new BitArray(map.nodes.Count);
                startNode.prevPathNodeIndex = -1;
                pathTree.Add(startNode);

                //compute the max length a path may have
                float LENGTH_THR = SimSettings.PATH_LENGTH_THR_MUL * (destNodePos - map.nodes[nextNode]).Length;

                while (pathTree.Count > 0)
                {
                    PathNode lastPathNode = pathTree[pathTree.Count - 1];
                    Float2 lastNodePos = map.nodes[lastPathNode.lastNodeIndex];
                    Float2 destDir = (destNodePos - lastNodePos).GetNormal();
                    int[] adjIndices = map.intersections[lastPathNode.lastNodeIndex].Indices;
                    pathBranch.Clear();
                    adjQualities.Clear();

                    for (int j = 0; j < adjIndices.Length; j++)//for each adjacent node
                    {
                        Float2 adjPos = map.nodes[adjIndices[j]];
                        Float2 adjDir = adjPos - lastNodePos;
                        float pathLength = lastPathNode.pathLength + adjDir.Length;
                        adjDir = adjDir.GetNormal();

                        if (!lastPathNode.crossedNodes[adjIndices[j]] && pathLength <= LENGTH_THR)
                        //if it has not been crossed yet and the whole path is not too long, it means that it is a possible node
                        {
                            //create a new path node
                            PathNode p = new PathNode();
                            p.lastNodeIndex = adjIndices[j];
                            p.crossedNodes = new BitArray(lastPathNode.crossedNodes);
                            p.crossedNodes.Set(lastPathNode.lastNodeIndex, true);
                            p.prevPathNodeIndex = pathTree.Count - 1;
                            p.pathLength = pathLength;

                            //sort the nodes to add in <pts> so that at the end of <pts> there will be the most desirable
                            int adjIndex = 0;
                            float adjQuality = Float2.DotProduct(destDir, adjDir);
                            for (; adjIndex < pathBranch.Count && adjQuality > adjQualities[adjIndex]; adjIndex++) ;
                            pathBranch.Insert(adjIndex, p);
                            adjQualities.Insert(adjIndex, adjQuality);
                        }
                    }

                    if (pathBranch.Count == 0)
                    //from now on all paths are not desirable either because we're stuck or because they are too long
                    {
                        for (int curLast = pathTree.Count - 1, prevNodeIndex = curLast; prevNodeIndex == curLast && curLast >= 0; curLast--)
                        //backtracking
                        {
                            prevNodeIndex = pathTree[curLast].prevPathNodeIndex;
                            pathTree.RemoveAt(curLast);
                        }
                        continue;
                    }

                    //if the vehicle is turned in the opposite direction to the destination, correct the path
                    int curPrevNode = lastPathNode.prevPathNodeIndex < 0 ? prevNode : pathTree[lastPathNode.prevPathNodeIndex].lastNodeIndex;
                    if (curPrevNode == pathBranch[pathBranch.Count - 1].lastNodeIndex)
                    //if the vehicle tries to U-turn
                    {
                        PathNode uturnPath = pathBranch[pathBranch.Count - 1];
                        pathBranch.RemoveAt(pathBranch.Count - 1);
                        pathBranch.Insert(0, uturnPath);

                        //since its going in a wrong direction, unlock previuos nodes to make possible for the vehicle to come back if needed
                        foreach (PathNode n in pathBranch)
                        {
                            n.crossedNodes[lastPathNode.lastNodeIndex] = false;
                        }
                    }

                    //check if a path is linked to the destination
                    foreach (PathNode n in pathBranch)
                    {
                        //If the initial intersection is the destination, we have to avoid an U-turn
                        if (pathTree.Count == 1 && (prevNode == destNode) && n.lastNodeIndex == destNode && pathBranch.Count > 1) break;

                        if (n.lastNodeIndex == destNode)
                        //destination reached, return the path
                        {
                            Stack<Float2> path = new Stack<Float2>();
                            PathNode curNode = n;
                            Float2 posNodeToAddInPath;
                            while (curNode.prevPathNodeIndex != -1)
                            {
                                posNodeToAddInPath = map.nodes[curNode.lastNodeIndex];
                                path.Push(posNodeToAddInPath);
                                curNode = pathTree[curNode.prevPathNodeIndex];
                            }
                            posNodeToAddInPath = map.nodes[curNode.lastNodeIndex];
                            path.Push(posNodeToAddInPath);
                            return path.ToArray();
                        }
                    }

                    //add the new nodes to tree
                    pathTree.AddRange(pathBranch);
                }
                return null;
            }

            /// <summary>
            /// Compute a good path to reach the destNode from the initial position.
            /// </summary>
            public Float2[] ComputeGPSPath(Float2 prevNode, Float2 nextNode, Float2 destNode)
            {
                float dist;
                int pn = map.nodes.GetClosestNode(prevNode, out dist);
                int nn = map.nodes.GetClosestNode(nextNode, out dist);
                int dn = map.nodes.GetClosestNode(destNode, out dist);
                return ComputeGPSPath(pn, nn, dn);
            }
        }

    }
}
