﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation.Attuators;
using LRTrafficSimulator.Simulation.Environments;
using LRTrafficSimulator.Simulation.Robots;
using LRTrafficSimulator.Simulation.Sensors;

namespace LRTrafficSimulator.Simulation.Robots
{
    public class Vehicle : Robot
        //This class models a vehicle, with its sensors and attuators
    {
        public static readonly float VEHICLE_WIDTH = 30f;
        public static readonly float VEHICLE_LENGTH = 10f;
        private static readonly Float2 QS_POS = new Float2(VEHICLE_WIDTH / 2, 0f);
        private static readonly Float2 NO_POS = new Float2();
        private static readonly Float2 WHEELS_POS = new Float2(VEHICLE_WIDTH / 4, 0f);

        public Vehicle(SensorsFactory sf, AttuatorsFactory af) : base(TCRSensors(sf), TCRAttuators(af))
        {
            
        }

        //Creates and returns the sensors' list
        private static List<ISensor> TCRSensors(SensorsFactory sf)
        {
            List<ISensor> tcrSensors = new List<ISensor>();

            tcrSensors.Add(sf.CreateQueuedSensor("Queued Sensor", false, QS_POS));
            tcrSensors.Add(sf.CreateGPSTopologySensor("GPS Topology Sensor", new IntersectionInfo(), NO_POS));
            tcrSensors.Add(sf.CreateRightOfWaySensor("Semaphore Sensor", true, NO_POS));
            tcrSensors.Add(sf.CreateGPSPositionSensor("GPS Position Sensor", new Float2(), NO_POS));
            tcrSensors.Add(sf.CreateGPSDirectionSensor("GPS Direction Sensor", new Float2(1f, 0f), NO_POS));
            tcrSensors.Add(sf.CreateIntSensor("Int Sensor", false, NO_POS));
            tcrSensors.Add(sf.CreatePathSensor("Path Sensor", new Float2[2], NO_POS));
            tcrSensors.Add(sf.CreateDestReachedSensor("Dest Reached Sensor", false, NO_POS));
            tcrSensors.Add(sf.CreateClockSensor("Time", 0, NO_POS));
            tcrSensors.Add(sf.CreateTrafficSensor("Traffic Sensor", new TrafficInfo(), NO_POS));

            return tcrSensors;
        }

        //Creates and returns the attuators' list 
        private static List<IAttuator> TCRAttuators(AttuatorsFactory af)
        {
            List<IAttuator> tcrAttuators = new List<IAttuator>();

            tcrAttuators.Add(af.CreateDoubleLinearMotor("Wheels", new Float2(), WHEELS_POS, new Float2(0f), VEHICLE_LENGTH));
            tcrAttuators.Add(af.CreateNavigationGuide("Navigation Guide", false, NO_POS));
            tcrAttuators.Add(af.CreateParkingAttuator("Parking Attuator", false, NO_POS));
            tcrAttuators.Add(af.CreateNavigatorTransmitter("Navigator Transmitter", 0f, NO_POS));
            tcrAttuators.Add(af.CreateNavigatorUpdater("Navigator Updater", new PathUpdatingInfo(), NO_POS));

            return tcrAttuators;
        }

        public override string ToString()
        {
            return "Vehicle (Robot)";
        }
    }
}
