﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation.Environments;

namespace LRTrafficSimulator.Simulation
{
    public class DiscreteTimeSimulator : ISimulator
        //This class models the traffic simulator
    {
        private IEnvironment env;
        private List<Robot> robots;
        private List<VehicleState> robotStates;
        private Thread simThread;
        private bool stopRequest, running;
        public event Action SimulationStepPerformed;
        public event Action Stopped;

        //Creates a new DiscreteTimeSimulator given an IEnvironment
        public DiscreteTimeSimulator(IEnvironment env)
        {
            this.env = env;
            this.robots = new List<Robot>();
            this.robotStates = new List<VehicleState>();
        }

        public bool IsRunning
        {
            get
            {
                return running;
            }
        }

        public bool Stopping
        {
            get
            {
                return stopRequest;
            }
        }

        //Perform a single simulation step
        private void simulationStep()
        {
            running = true;

            while (!stopRequest)
            {          
                //updates sensors' values
                for (int i = 0; i < robots.Count; i++)
                {
                    SimGlobal.CurrentRobot = i;
                    updateSensors(robots[i], robotStates[i]);
                }

                //executes a single step for every robot
                for (int i = 0; i < robots.Count; i++)
                {
                    SimGlobal.CurrentRobot = i;
                    robots[i].Step();
                }

                //updates the vehicle states and executes a single step on the attuators
                for(int i = 0; i < robots.Count; i++)
                {
                    SimGlobal.CurrentRobot = i;
                    VehicleState curState = robotStates[i];
                    commitAttuatorChanges(robots[i], ref curState);
                    robotStates[i] = curState;
                }

                //waits that gui has finished to draw the previous simulation frame
                SimGlobal.DrawSemaphore.WaitOne();
                SimulationStepPerformed.Invoke();
                SimGlobal.Time.NextSimFrame();
            }

            stopRequest = false;
            Stopped.Invoke();
            running = false;
        }

        private void commitAttuatorChanges(Robot r, ref VehicleState rs)
        {  
            foreach(IAttuator a in r.Attuators.Values)
            {
                a.commitAttuatorChanges(this.env, ref rs, SimGlobal.Time.StepTimeSeconds);
            }
        }

        private void updateSensors(Robot r, VehicleState rs)
        {
            foreach (ISensor s in r.Sensors.Values)
            {
                s.updateSensorValue(this.env, rs, SimGlobal.Time.StepTimeSeconds);
            }
        }

        public void AddRobot(Robot r, VehicleState s)
        {
            robots.Add(r);
            robotStates.Add(s);
        }

        public void StartSimulation()
        {
            simThread = new Thread(new ThreadStart(simulationStep));
            stopRequest = false;
            simThread.Start();
        }

        public void StopSimulation(bool syncWait)
        {
            stopRequest = true;
            if (syncWait && simThread != null)
            {
                simThread.Join();
            }
        }

        public void ClearRobots()
        {
            robots.Clear();
            robotStates.Clear();
        }

        public int RobotCount
        {
            get 
            {
                return robots.Count;
            }
        }

        public VehicleState GetRobotState(int index)
        {
            return robotStates[index];
        }

        public List<VehicleState> GetRobotStates()
        {
            return new List<VehicleState>(this.robotStates);
        }

        public Robot GetRobotAt(int index)
        {
            return this.robots[index];
        }
    }

    public struct VehicleState
    {
        public Float2 Position;
        public Float2 Direction;
        public Float2[] Path;
        public int NextNode;
        public Float2 FinalPosition;
        public CityMap.Navigator Nav;

        public VehicleState(Float2 position, Float2 direction)
        {
            Position = position;
            Direction = direction;
            Path = null;
            NextNode = 0;
            FinalPosition = new Float2();
            Nav = null;
        }
    }
}
