﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;

namespace LRTrafficSimulator.Simulation
{
    public abstract class Attuator<T> : IAttuator
    {
        protected T value;
        private string name;
        private Float2 pos;

        //Creates a new Attuator given a <name>, an <initialValue> and a certain <pos>
        public Attuator(string name, T initialValue, Float2 pos)
        {
            this.name = name;
            this.value = initialValue;
            this.pos = pos;
        }

        //Sets this Attuator value
        public void setAttuatorValue(T value)
        {
            this.value = value;
        }

        //Gets or sets this Attuator name
        public string Name
        {
            get
            {
                return name;
            }
        }

        //Gets or sets this Attuator position
        public Float2 Position
        {
            get
            {
                return pos;
            }
        }

        public void setAttuatorValue(object value)
        {
            if (value is T)
            {
                setAttuatorValue((T)value);
            }
            else
            {
                throw new ArgumentException("The object 'value' is not compatible with this attuator");
            }
        }


        public abstract void commitAttuatorChanges(IEnvironment env, ref VehicleState rs, float seconds);

    }
}
