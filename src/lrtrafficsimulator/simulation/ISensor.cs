﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation
{
    public interface ISensor
    {
        object getSensorValue();

        string Name { get; }

        Float2 Position { get; }

        void updateSensorValue(IEnvironment env, VehicleState rs, float seconds);
    }
}
