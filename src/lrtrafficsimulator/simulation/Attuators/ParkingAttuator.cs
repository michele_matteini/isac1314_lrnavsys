﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;

namespace LRTrafficSimulator.Simulation.Attuators
{
    public class ParkingAttuator : Attuator<bool>
    {
        public ParkingAttuator(string name, bool initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {

        }

        //if DestReachedSensor has perceived that the vehicle had reached its destination, this attuator moves it to its FinalPosition
        public override void commitAttuatorChanges(IEnvironment env, ref VehicleState rs, float seconds)
        {
            if (value)
            {
                rs.Position = rs.FinalPosition;
                rs.Direction = new Float2(90);
            }
        }
    }
}
