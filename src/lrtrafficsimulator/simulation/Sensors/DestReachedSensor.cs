﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Sensors
{
    public class DestReachedSensor : Sensor<bool>
    {
        public DestReachedSensor(string name, bool initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {}

        //perceives if vehicle has come to destination
        public override void updateSensorValue(IEnvironment env, VehicleState rs, float seconds)
        {
            if (rs.Path != null)
            {
                value = (rs.Path.Length == rs.NextNode);
            }
            else
            {
                value = false;
            }
        }
    }
}
