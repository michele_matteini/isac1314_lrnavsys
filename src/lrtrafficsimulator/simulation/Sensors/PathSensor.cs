﻿using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation.Environments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Sensors
{
    public class PathSensor : Sensor<Float2[]>
    {
        public PathSensor(string name, Float2[] initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {}

        //perceives both the position of the next intersection towards which the vehicle is moving and that of the destination
        public override void updateSensorValue(IEnvironment env, VehicleState rs, float seconds)
        {
            if (rs.Path != null && rs.NextNode < rs.Path.Length)
            {
                value[0] = rs.Path[rs.NextNode];
                value[1] = rs.Path[rs.Path.Length - 1];
            }
            else
            {
                value[0] = new Float2();
                value[1] = new Float2();
            }
            
        }
    }
}
