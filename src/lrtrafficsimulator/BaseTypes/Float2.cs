﻿using LRTrafficSimulator.Simulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.BaseTypes
{
    //This class models a 2D float
    public struct Float2
    {
        public float X, Y;

        //Creates a new Float2 from <x>, <y> coordinates
        public Float2(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }

        //Creates a new Float2 from a degree value
        public Float2(float degree)
        {
            float rad = degree / 180.0f * (float)Math.PI;
            X = (float)Math.Cos(rad);
            Y = (float)Math.Sin(rad);
        }

        //Gets the degree value of this Float2 between -180° and 180° 
        public float Degrees
        {
            get
            {
                return Radians * 180f / (float)Math.PI;
            }
        }

        //Gets the radians value of this Float2 between -π and π 
        public float Radians
        {
            get
            {
                if (Length > float.Epsilon)
                {
                    float rad = (float)Math.Acos(X / Length);
                    return Y < 0 ? -rad : rad;
                }
                else
                {
                    return 0;
                }
            }
        }

        //Gets the length of this Float2
        public float Length
        {
            get
            {
                return (float)Math.Sqrt(X * X + Y * Y);
            }
        }

        //Gets the distance between this Float2 and <p>
        public float DistanceFrom(Float2 p)
        {
            float dx = p.X - X;
            float dy = p.Y - Y;
            return (float)Math.Sqrt(dx * dx + dy * dy);
        }

        //Returns a new Float2 obtained doing the sum between <f1> and <f2>
        public static Float2 operator +(Float2 f1, Float2 f2)
        {
            return new Float2(f1.X + f2.X, f1.Y + f2.Y);
        }

        //Returns a new Float2 obtained doing the subtraction between <f1> and <f2>
        public static Float2 operator -(Float2 f1, Float2 f2)
        {
            return new Float2(f1.X - f2.X, f1.Y - f2.Y);
        }

        //Returns a new Float2 obtained multiplying <f1> and <f2>
        public static Float2 operator *(Float2 f1, Float2 f2)
        {
            return new Float2(f1.X * f2.X, f1.Y * f2.Y);
        }

        //Returns a new Float2 obtained multiplying <f> and the constant value <k>
        public static Float2 operator *(Float2 f, float k)
        {
            return new Float2(f.X * k, f.Y * k);
        }

        //Returns a new Float2 obtained multiplying the constant value <k> and <c1>  
        public static Float2 operator *(float k, Float2 f)
        {
            return new Float2(f.X * k, f.Y * k);
        }

        //Returns a new Float2 obtained dividing <f1> by <f2>
        public static Float2 operator /(Float2 f1, Float2 f2)
        {
            return new Float2(f1.X / f2.X, f1.Y / f2.Y);
        }

        //Returns a new Float2 obtained dividing <f1> by the constant value <k>
        public static Float2 operator /(Float2 f, float k)
        {
            return new Float2(f.X / k, f.Y / k);
        }

        //Computes the dot product between <f1> and <f2>
        public static float DotProduct(Float2 f1, Float2 f2)
        {
            return f1.X * f2.X + f1.Y * f2.Y;
        }

        //Computes the squared distance between <f1> and <f2>
        public static float SquaredDistance(Float2 f1, Float2 f2)
        {
            float dx = f1.X - f2.X;
            float dy = f1.Y - f2.Y;
            return dx * dx + dy * dy;
        }

        //Computes the angle in degree between <f1> and <f2>
        public static float DeltaDegrees(Float2 f1, Float2 f2)
        {
            float d1 = f1.Degrees, d2 = f2.Degrees;
            float deg = d2 > 0 ? d2 - d1 : 360f - d1 + d2;
            deg += deg > 360f ? -360f : (deg < 0 ? 360 : 0);
            return deg;
        }

        //Returns a new Float2 which is a normalized version of this
        public Float2 GetNormal()
        {
            return this / this.Length;
        }

        //Gets the hash code of this Float2
        public override int GetHashCode()
        {
            return (X.GetHashCode() >> 7) ^ Y.GetHashCode();
        }

    }
}
