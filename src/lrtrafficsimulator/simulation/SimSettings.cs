﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation
{
    public static class SimSettings
    {
        //simulator
        public const float DEFAULT_STEP_TIME = 0.1f;
        public const int MAX_FPS = 24;
        public const long FPS_UPDATE_TIME_MS = 1000;
    
        //traffic project
        public const float INTERS_RANGE = 20.0f;//range from which an intersection is visible
        public const float SEMAPHORE_RANGE = 60.0f;//range from which a semaphore is visible
        public static readonly Color3f ROBOT_COLOR = new Color3f(1, 0.2f, 0);
        public static readonly Color3f SEL_ROBOT_COLOR = new Color3f(1f, 0.8f, 0);
        public const float VEHICLE_SPEED = 23.0f;//vehicle speed in m/s
        public const float SEMAPHORE_TIME = 20.0f;//duration of green and red lights
        public const float SEMAPHORE_WAIT_TIME = 5.0f;//time between green lights switch
        public const float SEMAPHORE_P = 0.2f;//probability of an intersection to be controller by a semaphore
        public const float PATH_LENGTH_THR_MUL = 10f;//max path length multiplier
        public const int CARS_PARKING_PADDING = 20;//distance between two parked cars
        public static float TRAFFIC_INFO_RANGE { get; set; }//range where a vehicle perceives informations about traffic

        public const float ROUNDABOUT_DEF_RADIUS = 120f;//defalult radius of a roundabout
        public const float ROUNDABOUT_P = 0.1f;//probability of an intersection to be controller by a semaphore

        public const float NAV_PACKET_PERSISTENCE = 21.0f;//time after which the packet expires
        public const float NAV_PACKET_DT = 2.9f;//time between each signal

        //random map params
        public const float NET_PERTURBATION = 0.6f;//tra 0 e 1
        public const float REJECT_P = 0.1f;//probabilità che una strada nn venga aggiunta alla griglia
        public const int BLOCK_REJECT_COUNT = 7;//numero di zone senza strade
        public const double MAP_DIM_MUL = 200f;//default map dim multiplier
    }
}
