﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation.Environments;

namespace LRTrafficSimulator.Simulation
{
    public class RightOfWaySensor : Sensor<bool>
    {
        private const float FREE_WAY_TIME = 4.0f;

        private float freeTimeLeft;

        public RightOfWaySensor(string name, bool initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {}

        //if the vehicle is next to an intersection, this sensor perceives if it has to give way to other vehicles
        //either because of a red traffic light or because of a give way road signal
        public override void updateSensorValue(IEnvironment env, VehicleState rs, float seconds)
        {
            //Transformed sensor position
            float sampleDeg = rs.Direction.Degrees + this.Position.Degrees;
            Float2 rotSensorOffset = new Float2(sampleDeg) * this.Position.Length;
            Float2 semSensorPos = rotSensorOffset + rs.Position;

            CityTile tile = (CityTile)env.SampleEnvInformations(semSensorPos);
            value = true;
            if (tile.Info.GreenLight == null) return;

            //find current vehicle way
            Float2 intPos = tile.Info.IntersectionPos;
            Float2[] adjacentNodes = tile.Info.Positions;
            float maxDot = -1f - float.Epsilon;
            int curWay = -1;
            float movingDir = -1f;
            for (int i = 0; i < adjacentNodes.Length; i++)
            {
                float dot = Float2.DotProduct(rs.Direction * movingDir, (adjacentNodes[i] - intPos).GetNormal());
                if (dot > maxDot)
                {
                    maxDot = dot;
                    curWay = i;
                }
            }

            switch (tile.Info.Type)
            {
                case IntersectionType.None:
                    freeTimeLeft = 0;
                    value = true;
                    break;
                case IntersectionType.Semaphored:
                    value = tile.Info.GreenLight[curWay];
                    break;
                case IntersectionType.GiveWay:
                    freeTimeLeft-= seconds;
                    if (freeTimeLeft > 0)
                    {
                        if ((tile.Info.IntersectionPos - rs.Position).Length < (SimSettings.SEMAPHORE_RANGE - 20f))
                            break;//a car blocking an intersection has to release it

                        value = false;
                    }
                    else
                    {
                        if (maxDot < 0f) break;//if the vehicle is inside a roundabout, it never has to give way

                        if (!tile.Info.GreenLight[curWay])
                        {
                            CityTile ti = new CityTile();
                            for (int i = 0; i < adjacentNodes.Length && value; i++)
                            {
                                if (i == curWay) continue;
                                Float2 offset = (adjacentNodes[i] - intPos).GetNormal();
                                ti = (CityTile)env.SampleEnvInformations(intPos + offset * SimSettings.SEMAPHORE_RANGE);
                                value = value && !(ti.QueuedCars && tile.Info.GreenLight[i]);
                                ti = (CityTile)env.SampleEnvInformations(intPos + offset * SimSettings.INTERS_RANGE);
                                value = value && !(ti.QueuedCars && tile.Info.GreenLight[i]);
                            }

                            if (ti.Info.GiveWayTo != null)
                            {
                                Float2 offset = (ti.Info.GiveWayTo[0] - intPos).GetNormal();
                                ti = (CityTile)env.SampleEnvInformations(intPos + offset * SimSettings.SEMAPHORE_RANGE);
                                value = value && !ti.QueuedCars;
                                ti = (CityTile)env.SampleEnvInformations(intPos + offset * SimSettings.INTERS_RANGE);
                                value = value && !ti.QueuedCars;
                            }

                            if (value)
                            {
                                freeTimeLeft = 0f;
                            }
                            else
                            {
                                freeTimeLeft = FREE_WAY_TIME;
                                value = false;
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }

    }
}
