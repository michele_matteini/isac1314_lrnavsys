﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Sensors
{
    public class ClockSensor : Sensor<float>
    {
        public ClockSensor(string name, float initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {
        }

        //measures the simulation elapsed time 
        public override void updateSensorValue(IEnvironment env, VehicleState rs, float seconds)
        {
            this.value += seconds;
        }
    }
}
