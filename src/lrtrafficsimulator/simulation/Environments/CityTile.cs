﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Environments
{
    public struct CityTile
    {
        public bool QueuedCars;
        public bool Intersection;
        public IntersectionInfo Info;
        public Float2 AheadCarDir;

        public override string ToString()
        {
            return "Queued Cars:" + QueuedCars.ToString() + ", Ahead Car Dir: " + AheadCarDir + ", Intersection info: " + Info.ToString();
        }
    }
}
