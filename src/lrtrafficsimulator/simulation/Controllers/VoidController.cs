﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Controllers
{
    public class VoidController : Controller
    {
        public VoidController()
        {
        }

        //It does anything
        public override void Execute(Dictionary<string, ISensor> sensors, Dictionary<string, IAttuator> attuators)
        {
            System.Threading.Thread.SpinWait(10);
        }
    }
}
