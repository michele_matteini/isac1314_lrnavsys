﻿using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.GUI;
using LRTrafficSimulator.Simulation;
using LRTrafficSimulator.Simulation.Attuators;
using LRTrafficSimulator.Simulation.Environments;
using LRTrafficSimulator.Simulation.Robots;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator
{
    namespace GUI
    {
        //This class draws all the traffic simulation elements
        public class TrafficDrawer : SimDrawer
        {
            private TrafficGfxResources res;

            //Creates a new TrafficDrawer given an ISimulator and an IEnvironment
            public TrafficDrawer(ISimulator sim, IEnvironment env)
                : base(sim, env)
            {
                res.roadPen = new Pen(Color.FromArgb(160, 0, 0, 0), 1);
                res.road2Pen = new Pen(Color.FromArgb(160, 0, 0, 0), 1);

                res.bgRoadPen = new Pen(Color.FromArgb(160, 255, 174, 0), 1);
                res.bgRoad2Pen = new Pen(Color.FromArgb(160, 255, 174, 0), 1);

                res.pathPen = new Pen(Color.FromArgb(255, 0, 180, 255), 3f);

                res.bgImage = Image.FromFile("images/bgImage1.png");
                res.sunFlareImage = Image.FromFile("images/sunflare.png");
                res.semRedImage = Image.FromFile("images/sem-red.png");
                res.semGreenImage = Image.FromFile("images/sem-green.png");
                res.carImages = new Image[7];
                for (int i = 0; i < res.carImages.Length; i++)
                {
                    res.carImages[i] = Image.FromFile(String.Format("images/car_{0}.png", i + 1));
                }
                res.destImage = Image.FromFile("images/destIcon.png");
                res.carGlow = Image.FromFile("images/carGlow.png");
                res.bgTextureSize = 800f;
                res.bgLodThr = 0.10f;
                res.bgBrush = new TextureBrush(res.bgImage);
                res.bgBrush.WrapMode = WrapMode.Tile;
                res.giveWaySign = Image.FromFile("images/giveWay.png");
                res.scanBgBrush = new TextureBrush(Image.FromFile("images/scan1.png"));
                res.scanBgBrush.WrapMode = WrapMode.Tile;
            }

            //Creates a new TrafficDrawer given an ISimulator, an IEnvironment and 
            //a filepath from where load the elements that will be included in the map
            public TrafficDrawer(ISimulator sim, IEnvironment env, string mapFilePath)
                : this(sim, env)
            {
                //load map file
                string[] mapFile = File.ReadAllLines(mapFilePath);

                //skip to background loading
                int i = 0;
                i += int.Parse(mapFile[i].Split(';')[0]) + 1;
                i += int.Parse(mapFile[i]) + 1;
                i += int.Parse(mapFile[i]) + 1;

                if (mapFile.Length <= i) return;

                //load bg textures
                int lodCount = int.Parse(mapFile[i]);
                res.bgLodBrushes = new TextureBrush[lodCount];
                res.bgLodBrushesPos = new Float2[lodCount];
                res.bgLodBrushesScale = new float[lodCount];
                for (int li = 0; li < lodCount; li++)
                {
                    res.bgLodBrushes[li] = new TextureBrush(Image.FromFile(
                        mapFilePath.Substring(0, mapFilePath.Length - 4) + "_LOD" + li + ".jpg"
                        ));
                    res.bgLodBrushes[li].WrapMode = WrapMode.Clamp;
                    string[] bgBrushInfo = mapFile[i + 1 + li].Split(';');
                    res.bgLodBrushesPos[li] = new Float2(float.Parse(bgBrushInfo[0]), float.Parse(bgBrushInfo[1]));
                    res.bgLodBrushesScale[li] = float.Parse(bgBrushInfo[2]);
                }
                string[] bgColorChannels = mapFile[i + 1 + lodCount].Split(';');
                res.bgColorBrush = new SolidBrush(Color.FromArgb(int.Parse(bgColorChannels[0]), int.Parse(bgColorChannels[1]), int.Parse(bgColorChannels[2])));
            }

            //Draws a simulation frame. In particular, it draws the view currently selected
            public override void DrawSimulationFrame(System.Drawing.Graphics g)
            {
                switch (viewName)
                {
                    case "Map and vehicles":
                        ((CityMapWithVehicles)Environment).DrawTopology(g, res);
                        break;
                    case "Traffic gradient":
                        ((CityMapWithVehicles)Environment).DrawGradient(g, res);
                        break;
                }
                
            }
        }

        public struct TrafficGfxResources
        {
            public Pen roadPen, road2Pen, pathPen;
            public Image bgImage;
            public Image[] carImages;
            public Image sunFlareImage;
            public Image semRedImage;
            public Image semGreenImage;
            public float bgTextureSize;
            public float bgLodThr;
            public Image destImage;
            public float destTextureSize;
            public Image carGlow;
            public TextureBrush bgBrush;
            public Image giveWaySign;
            public TextureBrush scanBgBrush;
            public TextureBrush[] bgLodBrushes;
            public Float2[] bgLodBrushesPos;
            public float[] bgLodBrushesScale;
            public SolidBrush bgColorBrush;
            public Pen bgRoadPen;
            public Pen bgRoad2Pen;
        }
    }

    namespace Simulation.Environments
    {

        public partial class CityMapWithVehicles : CityMap
        {
            //Draws the topology and all the elements inside it
            public void DrawTopology(Graphics g, TrafficGfxResources res)
            {
                base.Draw(g, res);

                //draw destination points
                foreach (int destNode in destinations)
                {
                    int x1, y1;
                    DrawerScaling.Instance.RealCoordsToPixels(nodes[destNode], out x1, out y1);
                    g.DrawImage(res.destImage, x1 - 16, y1 - 54, 32, 54);
                }

                //draw vehicles
                int vwidth = (int)DrawerScaling.Instance.RealSizeToPixels(Vehicle.VEHICLE_WIDTH);
                int vheight = (int)DrawerScaling.Instance.RealSizeToPixels(Vehicle.VEHICLE_LENGTH * 1.2f);
                for (int i = 0; i < robotStates.Count; i++)
                {
                    int vx, vy;
                    DrawerScaling.Instance.RealCoordsToPixels(robotStates[i].Position, out vx, out vy);
                    float vdeg = robotStates[i].Direction.Degrees;

                    if (i == selRobotIndex)
                    //if i its the selected vehicle, draw its path
                    {
                        if (robotStates[i].Path == null) continue; //no path available

                        int rx1, ry1, rx2, ry2;
                        DrawerScaling.Instance.RealCoordsToPixels(robotStates[i].Position, out rx1, out ry1);
                        for (int s = robotStates[i].NextNode - 1; s < robotStates[i].Path.Length; s++)
                        {
                            DrawerScaling.Instance.RealCoordsToPixels(robotStates[i].Path[s], out rx2, out ry2);
                            g.DrawLine(res.pathPen, rx1, ry1, rx2, ry2);
                            rx1 = rx2;
                            ry1 = ry2;
                        }
                        SimGlobal.DrawRotatedImg(g, res.carGlow, vdeg, vx, vy, (int)(vheight * 1.8f), (int)(vwidth * 1.8f));
                    }

                    SimGlobal.DrawRotatedImg(g, res.carImages[i % res.carImages.Length], vdeg, vx, vy, vheight, vwidth);
                }

                g.DrawImage(res.sunFlareImage, g.VisibleClipBounds);
            }

            //draw the traffic gradient currently present within the topology 
            public void DrawGradient(Graphics g, TrafficGfxResources res)
            {
                //draw background
                float bgsize = DrawerScaling.Instance.RealSizeToPixels(10000);
                bgsize /= 500;
                int x0, y0;
                DrawerScaling.Instance.RealCoordsToPixels(new BaseTypes.Float2(), out x0, out y0);
                res.scanBgBrush.ResetTransform();
                res.scanBgBrush.TranslateTransform(x0, y0);
                res.scanBgBrush.ScaleTransform(bgsize, bgsize);
                g.FillRectangle(res.scanBgBrush, g.VisibleClipBounds);

                float maxGradValue = 0;
                for (int i = 0; i < diffusedData.Count; i++)
                {
                    SpreadData d = diffusedData[i];
                    NavPacket np = (NavPacket)d.data;
                    maxGradValue = Math.Max(np.GradientValue, maxGradValue);
                }

                if (maxGradValue <= 0) return;

                //draw gradient values
                SolidBrush sampleBrush = new SolidBrush(Color.Black);
                for (int i = 0; i < diffusedData.Count; i++)
                {
                    SpreadData d = diffusedData[i];
                    NavPacket np = (NavPacket)d.data;

                    int x, y, size;
                    DrawerScaling.Instance.RealCoordsToPixels(d.pos, out x, out y);
                    size = (int)DrawerScaling.Instance.RealSizeToPixels(40.0f);
                    float colorValue = Math.Min(np.GradientValue / maxGradValue, 1.0f);

                    Color brushColor = Color.FromArgb(
                        (int)(contrast(colorValue, 0.4f) * 255f),
                        (int)(Math.Pow(colorValue, 1.3) * 255f),
                        (int)((colorValue * 0.6f + 0.05f) * 255f)
                    );
                    
                    sampleBrush.Color = brushColor;
                    g.FillEllipse(sampleBrush, x - size / 2, y - size / 2, size, size);
                    sampleBrush.Color = Color.FromArgb(180, brushColor);
                    size = (int)(size * 1.4f);
                    g.FillEllipse(sampleBrush, x - size / 2, y - size / 2, size, size);
                    sampleBrush.Color = Color.FromArgb(100, brushColor);
                    size = (int)(size * 1.4f);
                    g.FillEllipse(sampleBrush, x - size / 2, y - size / 2, size, size);
                }
            }

            //Computes the constrast of <x> using a certain <power>
            private float contrast(float x, float power)
            {
                float sx = (x - 0.5f) * 2f;
                return (float)Math.Pow(Math.Abs(sx), power) * Math.Sign(sx) * 0.5f + 0.5f;
            }
        }

    }

}
