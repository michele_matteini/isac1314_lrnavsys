﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Controllers
{
    public class GoStraight : Controller
    {
        public GoStraight()
        {
        }

        //Moves the vehicle always straight on the current direction
        public override void Execute(Dictionary<string, ISensor> sensors, Dictionary<string, IAttuator> attuators)
        {
            attuators["Wheels"].setAttuatorValue(new Float2(1, 1));
        }
    }
}
