﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Environments
{
    public struct PathNode
    {
        public int lastNodeIndex;
        public BitArray crossedNodes;
        public int prevPathNodeIndex;
        public float pathLength;
    }
}
