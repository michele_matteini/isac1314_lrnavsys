﻿using LRTrafficSimulator.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LRTrafficSimulator.EntryPoints
{
    public static class TrafficSimulationEntryPoint
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //TODO list
            /*
             * 
             * 
             * 
             */

            Form simGUI = new FormTrafficSim();
            Application.Run(simGUI);
        }

    }
}
