﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation
{
    public abstract class Controller : IController
    {
        public event Action<String> ControlEvent;

        protected Controller()
        {
        }

        protected virtual void OnControlEvent(string eventDescr)
        {
            Action<String> handler = ControlEvent;
            if (handler != null)
            {
                handler(eventDescr);
            }
        }

        public abstract void Execute(Dictionary<string, ISensor> sensors, Dictionary<string, IAttuator> attuators);

    }
}
