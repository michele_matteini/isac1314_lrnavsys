﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;

namespace LRTrafficSimulator.Simulation.Attuators
{
    public class NavigatorUpdater : Attuator<PathUpdatingInfo>
    {
        public NavigatorUpdater(string name, PathUpdatingInfo initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {

        }

        private const float EQUAL_NODES_THR = 0.01f;
        //if the next intersection in path is not the one expected, the path has to be computed again
        public override void commitAttuatorChanges(IEnvironment env, ref VehicleState rs, float seconds)
        {
            if(rs.Path == null) return;

            if (value.IsAvailable && Float2.SquaredDistance(rs.Path[rs.NextNode - 1], value.NextNode) > EQUAL_NODES_THR)
            // computes the path again
            {
                rs.Path = rs.Nav.ComputeGPSPath(rs.Path[rs.NextNode - 2], value.NextNode, rs.Path[rs.Path.Length - 1]);
                rs.NextNode = 1;
            }
            value.IsAvailable = false;
        }
    }

    public struct PathUpdatingInfo
    {
        public Float2 NextNode;
        public bool IsAvailable;

        public PathUpdatingInfo(Float2 nextNode, bool isAvailable)
        {
            NextNode = nextNode;
            IsAvailable = isAvailable;
        }
    }
}
