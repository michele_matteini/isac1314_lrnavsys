﻿using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.GUI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Environments
{
    public partial class CityMap : SimEnvironment
    {
        protected BoolMatrix street;
        protected PositionSet nodes;
        protected List<Intersection> intersections;
        protected float mapRadius;
        private bool[] semState1, semState2, semRed, semGreen;
        protected HashSet<int> destinations;

        //Gets the node (intersection) count
        public int NodeCount
        {
            get
            {
                return nodes.Count;
            }
        }

        //Gets the map radius
        public float MapRadius
        {
            get
            {
                return mapRadius;
            }
        }

        //Gets or sets the destinations
        public int DestinationCount
        {
            get
            {
                return destinations.Count;
            }
            set
            {
                destinations.Clear();
                while (destinations.Count < value)
                {
                    int dest = 0, i = nodes.Count;
                    while (i == nodes.Count)
                    {
                        dest = SimGlobal.Random.Next(nodes.Count);
                        for (i = 0; i < nodes.Count && !street[dest, i]; i++) ;
                    }
                    destinations.Add(dest);
                }
            }
        }

        /// <summary>
        /// Create a random map based on the number of nodes.
        /// </summary>
        public CityMap(float mapRadius, int nodeCount)
        {
            this.mapRadius = mapRadius;
            destinations = new HashSet<int>();
            initializeSemaphores();
            initializeRandomMap(nodeCount);
            initializeIntersectionInfo();
            initializeRandomIntersections();
        }

        /// <summary>
        /// Create a map to which add nodes further
        /// </summary>
        public CityMap(string mapFilePath)
        {
            initializeSemaphores();

            //load map file
            string[] mapFile = File.ReadAllLines(mapFilePath);

            //initialize fields
            string[] mapData = mapFile[0].Split(';');
            int nodeCount = int.Parse(mapData[0]);
            float scaleX = float.Parse(mapData[1]);
            float scaleY = float.Parse(mapData[2]);
            nodes = new PositionSet();
            street = new BoolMatrix(nodeCount);       

            //initialize nodes
            mapRadius = 0;
            for (int i = 1; i < nodeCount + 1; i++)
            {
                string[] nodeData = mapFile[i].Split(' ');
                float nx = float.Parse(nodeData[0]) * scaleX, ny = float.Parse(nodeData[1]) * scaleY;
                nodes.AddPoint(nx, ny);
                nx = Math.Abs(nx);
                ny = Math.Abs(ny);
                mapRadius = mapRadius < nx ? (nx < ny ? ny : nx) : (mapRadius < ny ? ny : mapRadius);
            }
            nodes.ComputeIndex();

            //read streets between 2 nodes
            int streetCount = int.Parse(mapFile[nodeCount + 1]);
            for (int i = nodeCount + 2; i < streetCount + nodeCount + 2; i++)
            {
                string[] streetData = mapFile[i].Split(' ');
                int n1 = int.Parse(streetData[0]), n2 = int.Parse(streetData[1]);
                street[n1, n2] = true;
                if (bool.Parse(streetData[2])) street[n2, n1] = true;
            }

            initializeIntersectionInfo();

            //add intersection infos
            for (int i = 0; i < nodeCount; i++)
            {
                Intersection curInt = intersections[i];

                switch (mapFile[i + 1].Split(' ')[2])
                {
                    case "none":
                        curInt.Info.Type = IntersectionType.None;
                        curInt.Info.GreenLight = semGreen;
                        break;
                    case "gw"://give way
                        curInt.Info.Type = IntersectionType.GiveWay;
                        curInt.Info.GreenLight = semState2;
                        break;
                    case "row"://right of way
                        curInt.Info.Type = IntersectionType.GiveWay;
                        curInt.Info.GreenLight = semState1;
                        break;
                    case "sem"://semaphore
                        curInt.Info.Type = IntersectionType.Semaphored;
                        curInt.SemOffset = (float)SimGlobal.Random.NextDouble() * SimSettings.SEMAPHORE_TIME;
                        break;
                    case "ra"://roundabout
                        placeRoundabout(i);
                        break;
                    default:
                        throw new InvalidDataException(string.Format("Invalid token \"{0}\".", mapFile[i + 1].Split(' ')[2]));
                }
                intersections[i] = curInt;
            }

            //add destinations
            int destCount = int.Parse(mapFile[nodeCount + streetCount + 2]);
            destinations = new HashSet<int>();
            int desti = nodeCount + streetCount + 3;
            for (int i = desti; i < desti + destCount; i++)
            {
                int di = int.Parse(mapFile[i]);
                destinations.Add(di);
            }
        }

        private void initializeSemaphores()
        {
            this.semState1 = new bool[4] { true, false, true, false };
            this.semState2 = new bool[4] { false, true, false, true };
            this.semRed = new bool[4] { false, false, false, false };
            this.semGreen = new bool[4] { true, true, true, true };
        }

        public Float2 GetNodePosition(int id)
        {
            return nodes[id];
        }

        //Returns the environment informations in the particular <position>
        protected override object sampleEnvInformations(Float2 position)
        {
            CityTile tile = new CityTile();
            float distance;
            int nearestNode = nodes.GetClosestNode(position, out distance);


            if (distance < SimSettings.INTERS_RANGE)
            {
                tile.Intersection = true;
            }

            if (distance < SimSettings.SEMAPHORE_RANGE)
            {
                tile.Info = intersections[nearestNode].Info;
                if (intersections[nearestNode].Info.Type == IntersectionType.Semaphored)
                {
                    tile.Info.GreenLight = getSemaphoreState(nearestNode);
                }
            }

            return tile;
        }

        public bool[] getSemaphoreState(int index)
        {
            if (intersections[index].Info.Type != IntersectionType.Semaphored) return semGreen;//intersection with no traffic light

            float halfCycleTime = SimSettings.SEMAPHORE_TIME + SimSettings.SEMAPHORE_WAIT_TIME;
            float semCycleTime = (intersections[index].SemOffset + SimGlobal.Time.SimTimeSeconds) % (halfCycleTime * 2.0f);
            bool[] state = null;

            //no traffic lights for 2-way intersections
            if (intersections[index].Info.Positions.Length < 3) return semGreen;

            if (semCycleTime < halfCycleTime)
            {
                state = semState1;
            }
            else
            {
                semCycleTime -= halfCycleTime;
                state = semState2;
            }

            //semaphores go all red between 2 green lights
            if (semCycleTime > SimSettings.SEMAPHORE_TIME) state = semRed;

            return state;
        }

        //Draws the city topology
        public virtual void Draw(Graphics g, TrafficGfxResources res)
        {
            if (res.bgLodBrushes == null)
            {
                //draw background
                int x0, y0;
                DrawerScaling.Instance.RealCoordsToPixels(new Float2(0, 0), out x0, out y0);
                float s = (float)DrawerScaling.Instance.RealSizeToPixels(res.bgTextureSize) / (float)res.bgImage.Size.Width;
                res.bgBrush.ResetTransform();
                res.bgBrush.TranslateTransform(x0, y0);
                res.bgBrush.ScaleTransform(s, s);
                g.FillRectangle(res.bgBrush, g.VisibleClipBounds);
            }
            else
            {
                //draw custom bg
                g.FillRectangle(res.bgColorBrush, g.VisibleClipBounds);         
                for (int i = res.bgLodBrushes.Length - 1; i >= 0; i--)
                {
                    int x0, y0;
                    DrawerScaling.Instance.RealCoordsToPixels(res.bgLodBrushesPos[i], out x0, out y0);
                    float s = (float)DrawerScaling.Instance.RealSizeToPixels(res.bgLodBrushesScale[i] * 10f);
                    res.bgLodBrushes[i].ResetTransform();
                    res.bgLodBrushes[i].TranslateTransform(x0, y0);
                    res.bgLodBrushes[i].ScaleTransform(s, s);
                    g.FillRectangle(res.bgLodBrushes[i], g.VisibleClipBounds);
                }
            }


            //draw topology
            float roadSize = DrawerScaling.Instance.RealSizeToPixels(40f);
            Pen oneWayPen = res.bgLodBrushes == null ? res.roadPen : res.bgRoadPen;
            Pen twoWayPen = res.bgLodBrushes == null ? res.road2Pen : res.bgRoad2Pen;
            oneWayPen.Width = roadSize * 0.5f;
            twoWayPen.Width = roadSize;       

            for (int x = 1; x < nodes.Count; x++)
            {
                for (int y = 0; y < x; y++)
                {
                    bool street1, street2;
                    street1 = street[x, y];
                    street2 = street[y, x];

                    if (street1 || street2)
                    {
                        int rx1, ry1, rx2, ry2;
                        DrawerScaling.Instance.RealCoordsToPixels(nodes[x], out rx1, out ry1);
                        DrawerScaling.Instance.RealCoordsToPixels(nodes[y], out rx2, out ry2);
                        Pen rPen = street1 && street2 ? twoWayPen : oneWayPen;
                        g.DrawLine(rPen, rx1, ry1, rx2, ry2);
                    }
                }
            }

            //draw road signs
            for (int i = 0; i < nodes.Count; i++)
            {
                Float2[] npos = intersections[i].Info.Positions;
                       
                Float2 intPos = nodes[i];

                if (intersections[i].Info.Type == IntersectionType.Semaphored)
                {
                    if (npos.Length < 3) continue;  
                    bool[] sem = getSemaphoreState(i);

                    for (int si = 0; si < npos.Length; si++)
                    {
                        Image semImage = sem[si] ? res.semGreenImage : res.semRedImage;
                        Float2 semDir = (npos[si] - intPos).GetNormal();
                        Float2 semPos = intPos + semDir * 40f + new Float2(-semDir.Y, semDir.X) * 12f;
                        int sx1, sy1, semSize;
                        DrawerScaling.Instance.RealCoordsToPixels(semPos, out sx1, out sy1);
                        semSize = (int)DrawerScaling.Instance.RealSizeToPixels(30f);
                        SimGlobal.DrawRotatedImg(g, semImage, semDir.Degrees + 90, sx1, sy1, semSize, semSize);
                    }
                }
                else if (intersections[i].Info.Type == IntersectionType.GiveWay)
                {
                    bool[] rightOfWay = intersections[i].Info.GreenLight;
                    for (int si = 0; si < npos.Length; si++)
                    {
                        if (rightOfWay[si]) continue;

                        Float2 semDir = (npos[si] - intPos).GetNormal();
                        Float2 semPos = intPos + semDir * 30f/* + new Float2(-semDir.Y, semDir.X) * 12f*/;
                        int sx1, sy1, signSize;
                        DrawerScaling.Instance.RealCoordsToPixels(semPos, out sx1, out sy1);
                        signSize = (int)DrawerScaling.Instance.RealSizeToPixels(30f);
                        SimGlobal.DrawRotatedImg(g, res.giveWaySign, semDir.Degrees + 90, sx1, sy1, signSize, signSize);
                    }
                }
            }
        }

        //Initializes all the informations needed by vehicles to react to an intersection
        public void initializeIntersectionInfo()
        {
            intersections = new List<Intersection>();
            List<Float2> adjacentNodes;
            List<int> adjNodeIndices;
            for (int node = 0; node < nodes.Count; node++)
            {
                Intersection curInt = new Intersection();

                //initialize position
                Float2 intPos = nodes[node];
                curInt.Info.IntersectionPos = intPos;

                //initialize adjacent nodes
                adjacentNodes = new List<Float2>();
                adjNodeIndices = new List<int>();
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (street[node, i])
                    {
                        Float2 adjPos = nodes[i];
                        int ai = 0;
                        for (; ai < adjacentNodes.Count && ((adjacentNodes[ai] - intPos).Degrees < (adjPos - intPos).Degrees); ai++) ;
                        adjacentNodes.Insert(ai, nodes[i]);
                        adjNodeIndices.Insert(ai, i);
                    }
                }

                if (adjacentNodes.Count == 3)//sorting roads for 3-way semaphores
                {
                    Float2 n0 = (adjacentNodes[0] - intPos).GetNormal();
                    Float2 n1 = (adjacentNodes[1] - intPos).GetNormal();
                    Float2 n2 = (adjacentNodes[2] - intPos).GetNormal();
                    float d01 = (n0 - n1).Length, d02 = (n0 - n2).Length, d12 = (n1 - n2).Length;
                    if (d01 > d12)
                    {
                        if (d01 > d02)
                        {
                            //shift sx
                            adjacentNodes.Add(adjacentNodes[0]);
                            adjacentNodes.RemoveAt(0);
                            adjNodeIndices.Add(adjNodeIndices[0]);
                            adjNodeIndices.RemoveAt(0);
                        }
                    }
                    else if (d12 > d02)
                    {
                        //shift dx
                        adjacentNodes.Insert(0, adjacentNodes[2]);
                        adjacentNodes.RemoveAt(3);
                        adjNodeIndices.Insert(0, adjNodeIndices[2]);
                        adjNodeIndices.RemoveAt(3);
                    }
                }
                curInt.Info.Positions = adjacentNodes.ToArray();
                curInt.Indices = adjNodeIndices.ToArray();
                intersections.Add(curInt);
            }
        }

        private void initializeRandomMap(int nodeCount)
        {
            int n = (int)Math.Sqrt(nodeCount);
            n += (n * n == nodeCount) ? 0 : 1;//fix for perfect squares
            int n2 = n * n;

            float step = mapRadius * 2.0f / (float)(n - 1);
            float perturbation = step * (float)(SimGlobal.Random.NextDouble() - 0.5);
            nodes = new PositionSet();

            //create the intersection greed
            for (int xi = 0; xi < n; xi++)
            {
                float rxi = -mapRadius + step * xi;

                for (int yi = 0; yi < n; yi++)
                {
                    float ryi = -mapRadius + step * yi;

                    float rndx = step * (float)(SimGlobal.Random.NextDouble() - 0.5) * SimSettings.NET_PERTURBATION;
                    float rndy = step * (float)(SimGlobal.Random.NextDouble() - 0.5) * SimSettings.NET_PERTURBATION;

                    nodes.AddPoint(rxi + rndx, ryi +  rndy);
                }
            }

            //create the street map
            street = new BoolMatrix(n * n);
            for (int sx = 0; sx < nodes.Count; sx++)
            {
                for (int sy = 0; sy < sx; sy++)
                {
                    int diff2 = sx - sy;
                    diff2 *= diff2;
                    bool linked = (diff2 == 1 && sx / n == sy / n) || diff2 == n2;//uniform greed
                    street[sx, sy] = linked;
                    street[sy, sx] = linked;
                }
            }

            //clear random street blocks
            for (int bx = 0; bx < n; bx++)
            {
                for (int by = 0; by < n; by++)
                {
                    if (SimGlobal.Random.NextDouble() < SimSettings.REJECT_P)
                    {
                        int bi = bx * n + by;
                        street.ResetNode(bi);
                    }
                }
            }
        }

        //Initializes intersection with random road signs
        private void initializeRandomIntersections()
        {
            int nodeCount = nodes.Count;
            for (int node = 0; node < nodeCount; node++)
            {
                Intersection curInt = intersections[node];
                curInt.Info.GreenLight = semGreen;

                if (curInt.Indices.Length <= 2)
                //no intersection
                {
                    curInt.Info.Type = IntersectionType.None;

                }
                else
                {
                    float p = (float)SimGlobal.Random.NextDouble();
                    if (p < SimSettings.SEMAPHORE_P)
                    //initialize semaphore
                    {
                        
                        curInt.Info.Type = IntersectionType.Semaphored;
                        curInt.SemOffset = (float)SimGlobal.Random.NextDouble() * SimSettings.SEMAPHORE_TIME;
                    }
                    else if (p < SimSettings.ROUNDABOUT_P + SimSettings.SEMAPHORE_P)
                    //initialize roundabout
                    {
                        placeRoundabout(node);
                    }
                    else
                    //initialize give way
                    {
                        curInt.Info.Type = IntersectionType.GiveWay;
                        curInt.Info.GreenLight = curInt.Indices.Length < 4 || SimGlobal.Random.NextDouble() < 0.5 ? semState1 : semState2;
                    }
                }

                intersections[node] = curInt;
            }

            nodes.ComputeIndex();
        }

        //Create a roundabout centered in the <node> index
        private void placeRoundabout(int node)
        {
            Intersection curInt = intersections[node];
            street.ResetNode(node);

            //add roundabout nodes
            int[] raIndices = new int[curInt.Indices.Length * 2];
            for (int ai = 0; ai < curInt.Indices.Length; ai++)
            {
                Float2 dirNi = (curInt.Info.Positions[ai] - curInt.Info.IntersectionPos).GetNormal();

                //add roundabout main nodes
                Float2 niPos = curInt.Info.IntersectionPos + dirNi * SimSettings.ROUNDABOUT_DEF_RADIUS;
                nodes.AddPoint(niPos);
                raIndices[2 * ai] = street.AddNode();

                //add roundabout connection streets
                street[raIndices[2 * ai], curInt.Indices[ai]] = true;
                street[curInt.Indices[ai], raIndices[2 * ai]] = true;

                //add roundabout secondary nodes
                int nextAdj = (ai + 1) % curInt.Indices.Length;
                Float2 dirNi2 = (curInt.Info.Positions[nextAdj] - curInt.Info.IntersectionPos).GetNormal();
                float deltaDeg = Float2.DeltaDegrees(dirNi, dirNi2);
                float posCorrection = 1f - Math.Max(deltaDeg - 90f, 0) / 270f;
                Float2 ni2Pos = new Float2(dirNi.Degrees + deltaDeg / 2f) * posCorrection;
                ni2Pos = ni2Pos * SimSettings.ROUNDABOUT_DEF_RADIUS + curInt.Info.IntersectionPos;
                nodes.AddPoint(ni2Pos);
                raIndices[2 * ai + 1] = street.AddNode();
            }

            //add roundabout secondary streets
            for (int i = 0; i < raIndices.Length; i++)
            {
                //add roundabout internal connections
                int nexti = (i + 1) % raIndices.Length;
                street[raIndices[nexti], raIndices[i]] = true;
            }

            //update intersection infos
            for (int ai = 0; ai < curInt.Indices.Length; ai++)
            {
                //add roundabout main nodes
                Intersection int1 = new Intersection();
                int1.Indices = new int[] { raIndices[2 * ai + 1], curInt.Indices[ai]};
                int1.Info.Positions = new Float2[] { nodes[int1.Indices[0]], nodes[int1.Indices[1]] };
                int1.Info.GiveWayTo = new Float2[] { nodes[raIndices[(2 * ai - 1 + raIndices.Length) % raIndices.Length]] };
                int1.Info.GreenLight = semState1;
                int1.Info.IntersectionPos = nodes[raIndices[2 * ai]];
                int1.Info.Type = IntersectionType.GiveWay;
                intersections.Add(int1);

                //add roundabout secondary nodes
                Intersection int2 = new Intersection();
                int2.Indices = new int[] { raIndices[(2 * (ai + 1)) % raIndices.Length] };
                int2.Info.Positions = new Float2[] { nodes[int2.Indices[0]] };
                int2.Info.GreenLight = semGreen;
                int2.Info.IntersectionPos = nodes[raIndices[2 * ai + 1]];
                int2.Info.Type = IntersectionType.None;
                intersections.Add(int2);

                //update adjacent intersections
                int i = 0;
                int[] adjindices = intersections[curInt.Indices[ai]].Indices;
                Intersection extInt = intersections[curInt.Indices[ai]];
                while (i < adjindices.Length && adjindices[i] != node) { i++; }
                extInt.Indices[i] = raIndices[2 * ai];
                extInt.Info.Positions[i] = nodes[raIndices[2 * ai]];                          
            }

        }

    }
 
}
