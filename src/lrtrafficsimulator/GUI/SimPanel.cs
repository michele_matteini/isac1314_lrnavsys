﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using LRTrafficSimulator.BaseTypes;

namespace LRTrafficSimulator.GUI
{
    public class SimPanel : PictureBox
    {
        public event Action<Float2> SimMouseClick;

        private const int DEF_BG_SIZE = 100;//default background size

        ISimDrawer drawer;
        Graphics g;

        //Creates a new SimPanel from an ISimDrawer
        public SimPanel(ISimDrawer drawer)
        {
            this.drawer = drawer;
            this.BackgroundImage = new Bitmap(DEF_BG_SIZE, DEF_BG_SIZE);
            g = Graphics.FromImage(this.BackgroundImage);
        }

        //Gets or sets this ISimDrawer
        public ISimDrawer Drawer
        {
            get
            {
                return this.drawer;
            }
            set
            {
                this.drawer = value;
            }
        }

        //Draws a frame of the current simulation
        public void RefreshSimulation()
        {
            drawer.DrawSimulationFrame(g);
            g.DrawString(DisplayedInformation, SystemFonts.StatusFont, Brushes.Lime, new PointF(15, 15));
            this.Refresh();
        }

        //When the panel size changes, all the elements need to be scaled and the simulation needs to be refreshed
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.BackgroundImage = new Bitmap(this.Width, this.Height);
            g = Graphics.FromImage(this.BackgroundImage);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            DrawerScaling.Instance.TargetControlSize = this.Size;
            RefreshSimulation();
        }

        #region Movement and zoom

        private int mx, my;
        private RectangleF lastVisArea;

        //Gets or sets if the mouse is enabled
        public bool MouseEnabled
        {
            get;
            set;
        }

        //Gets or sets if the auto-refresh is enabled
        public bool AutoRefreshEnabled
        {
            get;
            set;
        }

        //When mouse is clicked, form will be notified
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            Float2 realCoord;
            DrawerScaling.Instance.PixelsToRealCoords(e.X, e.Y, out realCoord);
            SimMouseClick.Invoke(realCoord);
        }

        //When mouse is clicked, the last visible area is saved in order to modify it as soon as mouse will be moved
        private bool mouseDataAvailable = false;
        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (!MouseEnabled) return;

            if (e.Button == MouseButtons.Left)
            {
                mx = e.X;
                my = e.Y;
                lastVisArea = DrawerScaling.Instance.VisibleArea;
                mouseDataAvailable = true;
            }
        }

        //When mouse is moved, the visible area will be updated and gui will be refreshed
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (!MouseEnabled) return;

            if (e.Button == MouseButtons.Left && mouseDataAvailable)
            {
                float dx = mx - e.X;
                float dy = e.Y - my;
                RectangleF visArea = new RectangleF(new PointF(lastVisArea.X + dx * DrawerScaling.Instance.RealPixelSize(), lastVisArea.Y + dy * DrawerScaling.Instance.RealPixelSize()), lastVisArea.Size);
                DrawerScaling.Instance.VisibleArea = visArea;
            }
            base.OnMouseMove(e);
            if(AutoRefreshEnabled) RefreshSimulation();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            mouseDataAvailable = false;
        }

        //When mouse wheel is in use, the visible area will be zoomed (either in or out) and gui will be refreshed
        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (!MouseEnabled) return;

            if (e.Button != MouseButtons.Left)
            {
                lastVisArea = DrawerScaling.Instance.VisibleArea;
                float infAmount = -e.Delta * 0.0005f;
                infAmount = Math.Max(infAmount, -0.4f);
                lastVisArea.Inflate(lastVisArea.Width * infAmount, lastVisArea.Height * infAmount);
                DrawerScaling.Instance.VisibleArea = lastVisArea;

            }
            base.OnMouseWheel(e);
            if (AutoRefreshEnabled) RefreshSimulation();
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            if (!Focused) this.Focus();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            if (Focused) Parent.Focus();
        }

        #endregion

        public string DisplayedInformation { get; set; }
    }
}
