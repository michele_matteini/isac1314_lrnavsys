﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation
{
    public interface IAttuator
    {
        void setAttuatorValue(object value);

        string Name { get; }

        Float2 Position { get; }

        void commitAttuatorChanges(IEnvironment env, ref VehicleState rs, float seconds);
    }
}
