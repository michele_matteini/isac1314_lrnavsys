﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.Simulation;
using LRTrafficSimulator.Simulation.Environments;
using LRTrafficSimulator.Simulation.Robots;

namespace LRTrafficSimulator.GUI
{
    //This class needs to be overridden in order to draw the simulation components
    public abstract class SimDrawer : ISimDrawer
    {
        protected string viewName;

        //Creates a new SimDrawer given the references to an ISimulator and an IEnvironment
        public SimDrawer(ISimulator sim, IEnvironment env)
        {
            Simulator = sim;
            Environment = env;
        }

        //Returns a reference of an ISimulator
        protected ISimulator Simulator
        {
            get;
            private set;
        }

        //Returns a reference of an IEnvironment
        protected IEnvironment Environment
        {
            get;
            private set;
        }

        public abstract void DrawSimulationFrame(System.Drawing.Graphics g);

        //Sets the name of the view to draw
        public void SetView(string name)
        {
            this.viewName = name;
        }
    }
}
