﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Controllers
{
    public delegate void MicroBehaviour(Dictionary<string, ISensor> sensors, out float weight, out Float2 wspeed);

    public abstract class FuzzyStateController : Controller
        //This class models the basic logic of a controller
    {
        private int stateIndex;
        private List<List<MicroBehaviour>> states;
        private float timer;//state variable used in the micro behaviours of type TIMER

        //parameters
        private const float NO_OUTPUT = 0.01f;
        protected Dictionary<string, IAttuator> attuators;


        protected FuzzyStateController()
        {
            states = new List<List<MicroBehaviour>>();
            timer = -1;
        }

        //Executes a single step of robot simulation
        public override void Execute(Dictionary<string, ISensor> sensors, Dictionary<string, IAttuator> attuators)
        {
            this.attuators = attuators;

            //micro-behaviour results
            List<float> weights = new List<float>();
            List<Float2> wspeeds = new List<Float2>();
            float weight = 0;
            Float2 wspeed = new Float2();
            Action addResults = () => { weights.Add(weight); wspeeds.Add(wspeed); };

            //computes the behaviours, order by priority
            foreach (MicroBehaviour b in states[stateIndex])
            {
                b.Invoke(sensors, out weight, out wspeed);
                addResults();
            }

            //sums the behaviours' effects
            Float2 wspeedTot = new Float2();
            float totAttOutput = 0;
            for (int i = 0; i < weights.Count && totAttOutput < 1; i++)
            {
                wspeedTot += wspeeds[i] * weights[i];
                totAttOutput += weights[i];
            }
            attuators["Wheels"].setAttuatorValue(wspeedTot);

            //change the state of the FSM
            if (totAttOutput < NO_OUTPUT)
            {
                stateIndex = (stateIndex + 1) % states.Count;
                timer = -1;
            }
        }

        //Adds a state in FSM, associating it with the <behaviours> to consider in that state
        public int AddState(params MicroBehaviour[] behaviours)
        {
            int index = AddState();
            states[index].AddRange(behaviours);
            return index;
        }

        //Adds a state in FSM with no behaviour inside it
        public int AddState()
        {
            states.Add(new List<MicroBehaviour>());
            return states.Count - 1;
        }

        //Adds some <behaviours> to a previously created <state>
        public void AddBehaviours(int state, params MicroBehaviour[] behaviours)
        {
            states[state].AddRange(behaviours);
        }

        //Adds a <seconds> seconds timer to a previously created <state> 
        public void AddTimer(int state, float seconds)
        {
            states[state].Add(delegate(Dictionary<string, ISensor> sensors, out float weight, out Float2 wspeed){
                wspeed = new Float2();
                waitEvent(sensors, out weight, seconds);
            });
        }

        #region MICRO BEHAVIOUR: GENERIC WAIT
        private const float NEGATIVE_WEIGHT = -100.0f;
        private void waitEvent(Dictionary<string, ISensor> sensors, out float weight, float time)
        {
            if (timer < 0)
            {
                timer = (float)sensors["Time"].getSensorValue();
                weight = 0f;
            }
            else
            {
                float timeLeft = time - (float)sensors["Time"].getSensorValue() + timer;
                weight = (1f - timeLeft.Step(0)) * NEGATIVE_WEIGHT;
            }
        }
        #endregion

    }
}
