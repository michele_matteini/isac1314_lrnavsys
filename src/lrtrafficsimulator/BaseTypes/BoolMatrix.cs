﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.BaseTypes
{
    //This class models a squared boolean matrix
    public class BoolMatrix
    {
        private List<List<bool>> flags;

        //Creates a squared matrix of <size> * <size> bool initialized to false
        public BoolMatrix(int size)
        {
            flags = new List<List<bool>>(size); 
            bool[] initFalse = new bool[size];
            for (int i = 0; i < size; i++)
            {
                flags.Add(new List<bool>(initFalse));
            }
        }

        //Gets or sets the [x][y] element of the matrix
        public bool this[int x, int y]
        {
            get
            {
                return flags[x][y];
            }
            set
            {
                flags[x][y] = value;
            }
        }

        //Adds a row and a column to the matrix
        public int AddNode()
        {
            bool[] initFalse = new bool[flags.Count];
            flags.Add(new List<bool>(initFalse));
            for (int i = 0; i < flags.Count; i++)
            {
                flags[i].Add(false);
            }
            return flags.Count - 1;
        }

        //Sets to false all the elements having either row_index = <index> or column_index = <index>
        public void ResetNode(int index)
        {
            for (int n = 0; n < flags.Count; n++)
            {                
                flags[index][n] = false;
                flags[n][index] = false;
            }
        }

    }
}
