﻿using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation.Environments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.Simulation.Sensors;
using LRTrafficSimulator.Simulation.Attuators;

namespace LRTrafficSimulator.Simulation.Controllers
{
    public class TrafficAwareNavigator : FuzzyStateController
    {
        //vehicle state
        private bool intInfoAvailable;
        private Float2 intPos;//center of the intersection to be crossed
        private Float2 roadDir;//direction to be followed
        private Float2 termNodePos;//next intersection on the map

        private const float TURN_TIME = 1.50f;

        public TrafficAwareNavigator()
        {
            //street traveling
            AddState(queue, checkSemaphore, goStraight);

            //direction choice at an intersection
            AddState(checkDestination, selectPath);

            //computesthe path again (if it is necessary)
            AddState(updatePath);

            //turn travelling
            int curState = AddState();
            AddTimer(curState, TURN_TIME);
            AddBehaviours(curState, queue, turn);

            //signal the traffic level
            AddState(signalTrafficLevel);
        }

        #region  MICRO BEHAVIOUR: THE VEHICLE FOLLOWS THE SELECTED WAY
        private const float SAME_NODE_THR = 0.01f;
        private const float NO_POS_CORRECTION = 0.01f;
        private void goStraight(Dictionary<string, ISensor> sensors, out float weight, out Float2 wspeed)
        {
            Float2 vpos = (Float2)sensors["GPS Position Sensor"].getSensorValue();
            float pcorr = getPosCorrection(vpos);
            Float2 vdir = (Float2)sensors["GPS Direction Sensor"].getSensorValue();
            float dcorr = getDirCorrection(vdir);
            float correction = (pcorr < NO_POS_CORRECTION && pcorr > -NO_POS_CORRECTION) && intInfoAvailable ? dcorr : pcorr;
            
            wspeed = new Float2(1 - correction, 1 + correction) * SimSettings.VEHICLE_SPEED;


            bool intersection = (bool)sensors["Int Sensor"].getSensorValue();
            if (intersection)//check if it's the same intersection
            {
                IntersectionInfo info = (IntersectionInfo)sensors["GPS Topology Sensor"].getSensorValue();
                intersection = (intPos - info.IntersectionPos).Length > SAME_NODE_THR || !intInfoAvailable;
            }

            weight = intersection ? 0 : 1;
        }

        #endregion

        #region  MICRO BEHAVIOUR: THE VEHICLE STOPS IF IT HAS ANOTHER VEHICLE IN FRONT OF ITSELF

        private void queue(Dictionary<string, ISensor> sensors, out float weight, out Float2 wspeed)
        {
            wspeed = new Float2(0, 0);


            bool greenLight = (bool)sensors["Semaphore Sensor"].getSensorValue();
            IntersectionInfo info = (IntersectionInfo)sensors["GPS Topology Sensor"].getSensorValue();
            bool rightOfWay = info.GreenLight != null && greenLight && info.Type != IntersectionType.None;
            bool queued = (bool)sensors["Queued Sensor"].getSensorValue();

            weight = queued && !rightOfWay ? 1 : 0;
        }
        #endregion

        #region  MICRO BEHAVIOUR: THE VEHICLE STOPS IN FRONT OF A RED TRAFFIC LIGHT

        private void checkSemaphore(Dictionary<string, ISensor> sensors, out float weight, out Float2 wspeed)
        {
            wspeed = new Float2(0, 0);
            bool redLight = !(bool)sensors["Semaphore Sensor"].getSensorValue();
            IntersectionInfo info = (IntersectionInfo)sensors["GPS Topology Sensor"].getSensorValue();
            bool sameSemaphore = (intPos - info.IntersectionPos).Length < SAME_NODE_THR || !intInfoAvailable;

            weight = redLight && !sameSemaphore ? 1 : 0;
        }
        #endregion

        #region  MICRO BEHAVIOUR: THE VEHICLE CHOOSES THE WAY SUGGESTED BY NAVIGATOR USING THE CHOICE FUNCTION

        private void selectPath(Dictionary<string, ISensor> sensors, out float weight, out Float2 wspeed)
        {
            wspeed = new Float2(0, 0);
            weight = 0;

            bool intersection = (bool)sensors["Int Sensor"].getSensorValue();
            if (!intersection) return;

            IntersectionInfo info = (IntersectionInfo)sensors["GPS Topology Sensor"].getSensorValue();
            Float2[] nextInt = (Float2[])sensors["Path Sensor"].getSensorValue();
            int closestAdj = 0, currentAdj = 0;
            float closestAdjDist = (nextInt[0] - info.Positions[0]).Length;
            float closestCurAdjDist = (intPos - info.Positions[0]).Length;
            for (int i = 1; i < info.Positions.Length; i++)
            {
                float di = (nextInt[0] - info.Positions[i]).Length;
                if (di < closestAdjDist)
                {
                    closestAdj = i;
                    closestAdjDist = di;
                }

                float cur_di = (intPos - info.Positions[i]).Length;
                if (cur_di < closestCurAdjDist)
                {
                    currentAdj = i;
                    closestCurAdjDist = cur_di;
                }

            }

            TrafficInfo ti = (TrafficInfo)sensors["Traffic Sensor"].getSensorValue();
            closestAdj = SimGlobal.ChoiceFunction(closestAdj, closestCurAdjDist > 0.01f? -1 : currentAdj, nextInt[1], info.IntersectionPos, info.Positions, ti.DirectionGradients);

            intPos = info.IntersectionPos;
            roadDir = (info.Positions[closestAdj] - intPos).GetNormal();
            termNodePos = info.Positions[closestAdj];
            attuators["Navigation Guide"].setAttuatorValue(true);
            intInfoAvailable = true;
        }
        #endregion

        #region  MICRO BEHAVIOUR: THE VEHICLE COMPUTES THE PATH AGAIN

        private void updatePath(Dictionary<string, ISensor> sensors, out float weight, out Float2 wspeed)
        {
            wspeed = new Float2(0, 0);
            weight = 0;
            attuators["Navigator Updater"].setAttuatorValue(new PathUpdatingInfo(termNodePos,intInfoAvailable));
        }

        #endregion

        #region  MICRO BEHAVIOUR: THE VEHICLE CHECKS IF IT HAS REACHED ITS DESTINATION

        private bool destReached;
        private void checkDestination(Dictionary<string, ISensor> sensors, out float weight, out Float2 wspeed)
        {
            wspeed = new Float2(0, 0);
            if (!destReached)
            {
                destReached = (bool)sensors["Dest Reached Sensor"].getSensorValue();
                attuators["Parking Attuator"].setAttuatorValue(destReached);
                if (destReached)
                {
                    weight = 1;
                    OnControlEvent("Destination Reached");
                }
                else
                {
                    weight = 0;
                }
            }
            else
            {
                weight = 1;
            }
        
        }
        #endregion

        #region  MICRO BEHAVIOUR: THE VEHICLE TURNS IN THE NEW STREET

        private void turn(Dictionary<string, ISensor> sensors, out float weight, out Float2 wspeed)
        {
            Float2 vdir = (Float2)sensors["GPS Direction Sensor"].getSensorValue();
            float turnInt = getDirCorrection(vdir);

            wspeed = new Float2(1.0f - turnInt, 1.0f + turnInt) * SimSettings.VEHICLE_SPEED;
            weight = 1;
        }
        #endregion

        #region  MICRO BEHAVIOUR: THE VEHICLE SIGNAL THE TRAFFIC LEVEL

        private void signalTrafficLevel(Dictionary<string, ISensor> sensors, out float weight, out Float2 wspeed)
        {
            wspeed = new Float2(0, 0);
            weight = 0;
            TrafficInfo ti = (TrafficInfo)sensors["Traffic Sensor"].getSensorValue();
            attuators["Navigator Transmitter"].setAttuatorValue(ti.TrafficGradient);
        }
        #endregion

        #region TRAJECTORY CORRECTIONS IN ORDER TO FOLLOW STREETS

        private const float DIR_CORRECTION_AMOUNT = 1.8f;
        private float getDirCorrection(Float2 vdir)
        {
            float correction = roadDir.Degrees - vdir.Degrees;
            correction += correction > 180 ? -360 : (correction < -180 ? 360 : 0);
            correction = correction / 180 * DIR_CORRECTION_AMOUNT;
            return correction;
        }

        private const float POS_CORRECTION_AMOUNT = 0.001f;
        private float getPosCorrection(Float2 vpos)
        {
            float correction = 0;
            if (intInfoAvailable)//trajectory correction, based on the next intersection
            {
                float y0my1 = termNodePos.Y - intPos.Y;
                float x1mx0 = intPos.X - termNodePos.X;
                float x0y1 = termNodePos.X * intPos.Y;
                float x1y0 = intPos.X * termNodePos.Y;
                correction = (y0my1 * vpos.X + x1mx0 * vpos.Y + x0y1 - x1y0) / (float)Math.Sqrt(y0my1 * y0my1 + x1mx0 * x1mx0);
                correction = correction * POS_CORRECTION_AMOUNT;
            }

            return correction;
        }
        #endregion
    }
}
