﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;

namespace LRTrafficSimulator.Simulation
{
    public abstract class Sensor<T> : ISensor
    {
        protected T value;
        private string name;
        private Float2 pos;

        //Creates a new Sensor given a <name>, an <initialValue> and a certain <pos>
        public Sensor(string name, T initialValue, Float2 pos)
        {
            this.name = name;
            this.value = initialValue;
            this.pos = pos;
        }

        //Gets this Sensor value
        public T getSensorValue()
        {
            return value;
        }

        //Gets this Sensor name
        public string Name
        {
            get
            {
                return name;
            }
        }

        //Gets this Sensor position
        public Float2 Position
        {
            get
            {
                return pos;
            }
        }

        object ISensor.getSensorValue()
        {
            return getSensorValue();
        }

        public abstract void updateSensorValue(IEnvironment env, VehicleState rs, float seconds);
    }
}
