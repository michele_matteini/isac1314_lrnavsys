﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Environments
{
    public struct Intersection
    {
        public IntersectionInfo Info;
        public float SemOffset;
        public int[] Indices;
    }

    public struct IntersectionInfo
    {
        public Float2 IntersectionPos;
        public Float2[] Positions;
        public bool[] GreenLight;
        public IntersectionType Type;
        public Float2[] GiveWayTo;

        public override string ToString()
        {
            return "Intersection Pos: " + IntersectionPos.ToString() + Positions.ToString();
        }
    }

    public enum IntersectionType
    {
        None,
        Semaphored,
        GiveWay
    }

}
