﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation
{
    public interface IController
    {
        void Execute(Dictionary<string, ISensor> sensors, Dictionary<string, IAttuator> attuators);

        event Action<String> ControlEvent; 

    }

}
