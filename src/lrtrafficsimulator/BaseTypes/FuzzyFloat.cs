﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.BaseTypes
{
    //This class models a float using fuzzy logic
    public static class FuzzyFloat
    {
        //Limits the <value> between 0 and 1
        public static float Saturate(this float value)
        {
            return value > 1 ? 1 : (value < 0 ? 0 : value);
        }

        //Returns 1f if <value> > <thr>; 0f otherwise
        public static float Step(this float value, float thr)
        {
            return value > thr ? 1 : 0;
        }
    }

    //This struct models a set using fuzzy logic
    public struct FuzzySet
    {
        //Creates a new FuzzySet from a <value>
        public FuzzySet(float value) : this()
        {
            Value = value;
            saturate();
        }

        //Gets the float value of this FuzzySet
        public float Value
        {
            get;
            private set;
        }

        //Converts this FuzzySet value to bool
        public bool ToBoolean()
        {
            return Value >= 0.5f;
        }
        
        //Limits the FuzzySet value between 0 and 1
        private void saturate()
        {
            Value = Value > 1 ? 1 : (Value < 0 ? 0 : Value);
        }

        //Returns 1f if <value> > <thr>; 0f otherwise
        public void Step(float thr)
        {
            Value = Value > thr ? 1 : 0;
        }

        //Returns a new FuzzySet doing the AND operation between <f1> and <f2>
        public static FuzzySet operator &(FuzzySet f1, FuzzySet f2)
        {
            return new FuzzySet(f1.Value * f2.Value);
        }

        //Returns a new FuzzySet doing the OR operation between <f1> and <f2>
        public static FuzzySet operator |(FuzzySet f1, FuzzySet f2)
        {
            return new FuzzySet(f1.Value + f2.Value);
        }

        //Returns a new FuzzySet applying the NOT operation to <f1>
        public static FuzzySet operator !(FuzzySet f1)
        {
            return new FuzzySet(1f - f1.Value);
        }

    }


}
