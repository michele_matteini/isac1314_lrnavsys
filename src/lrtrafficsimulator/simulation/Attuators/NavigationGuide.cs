﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Attuators
{
    public class NavigationGuide : Attuator<bool>
    {
        public NavigationGuide(string name, bool initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {

        }

        //signal if the vehicle crosses an intersection so that the PathSensor may sense the next one in path
        public override void commitAttuatorChanges(IEnvironment env, ref VehicleState rs, float seconds)
        {
            if (value)
            {
                if (rs.Path != null)
                {
                    rs.NextNode += rs.NextNode == rs.Path.Length ? 0 : 1;
                }
                value = false;
            }
        }
    }
}
