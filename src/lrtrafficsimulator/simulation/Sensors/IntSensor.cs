﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation.Environments;

namespace LRTrafficSimulator.Simulation.Sensors
{
    public class IntSensor : Sensor<bool>
    {
        public IntSensor(string name, bool initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {}

        //perceives if the vehicle is next to an intersection
        public override void updateSensorValue(IEnvironment env, VehicleState rs, float seconds)
        {
            //Transformed sensor position
            float sampleDeg = rs.Direction.Degrees + this.Position.Degrees;
            Float2 rotSensorOffset = new Float2(sampleDeg) * this.Position.Length;
            Float2 intSensorPos = rotSensorOffset + rs.Position;

            CityTile tile = (CityTile)env.SampleEnvInformations(intSensorPos);
            if (tile.Info.Positions != null && tile.Info.Positions.Length == 0)
            //When a vehicle cross an empty intersection for an error
            {
                value = false;
            }
            else
            {
                value = tile.Intersection;
            }
        }

    }
}
