﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Controllers
{
    public static class ControllersList
    {
        //Gets the controller names
        public static string[] GetControllerNames()
        {
            List<string> controllers = new List<string>();
            Type[] allClasses = Assembly.GetExecutingAssembly().GetTypes();
            for (int i = 0; i < allClasses.Length; i++)
            {
                if (allClasses[i].GetInterfaces().Contains(typeof(IController)) && !allClasses[i].IsAbstract)
                {
                    controllers.Add(allClasses[i].Name);
                }
            }
            return controllers.ToArray();
        }

        //Gets a controller given its <name>
        public static IController GetController(string name)
        {
            Type ctrlType = Type.GetType("LRTrafficSimulator.Simulation.Controllers." + name);
            return (IController)Activator.CreateInstance(ctrlType);
        }

    }
}
