﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;
using System.Drawing;
using LRTrafficSimulator.GUI;

namespace LRTrafficSimulator.Simulation.Robots
{
    public class VehicleShape : IBoundingSurface
    {
        private float width, height;

        //Creates a VehicleShape with a certain <width> and <height>
        public VehicleShape(float width, float height)
        {
            this.width = width;
            this.height = height;
        }

        //Evaluates the distance from <shapePos> to <samplePos> along the direction <shapeDir>
        public float DistanceFrom(Float2 samplePos, Float2 shapePos, Float2 shapeDir)
        {
            Float2 ldir = (shapePos - samplePos);
            float distance = ldir.Length;
            ldir = new Float2(ldir.Degrees - shapeDir.Degrees);

            Float2 vehicleTrace = new Float2(0.5f * height * (float)Math.Sin(ldir.Radians), 0.5f * width * (float)Math.Cos(ldir.Radians));
            distance -= vehicleTrace.Length;
            distance = distance < 0 ? 0 : distance;
            return distance;
        }
        
        //Draws the vehicle shape
        public void DrawShape(Float2 pos, Float2 dir, Graphics g, Color3f color)
        {
            int px, py;
            DrawerScaling.Instance.RealCoordsToPixels(new Float2(pos.X - width / 2, pos.Y + height / 2), out px, out py);
            int pixWidth, pixHeight;
            pixWidth = (int)DrawerScaling.Instance.RealSizeToPixels(width);
            pixHeight = (int)DrawerScaling.Instance.RealSizeToPixels(height);


            g.TranslateTransform(px + pixWidth / 2, py + pixHeight / 2);
            g.RotateTransform(-dir.Degrees);
            g.TranslateTransform(-px - pixWidth / 2, -py - pixHeight / 2);

            g.FillRectangle(new SolidBrush(color.ToGuiColor()), new Rectangle(px, py, pixWidth, pixHeight));
            g.ResetTransform();
        }

        public float DistanceFrom(Float2 samplePos, Float2 sampleDir, Float2 shapePos, Float2 shapeDir)
        {
            throw new NotImplementedException();
        }
    }
}
