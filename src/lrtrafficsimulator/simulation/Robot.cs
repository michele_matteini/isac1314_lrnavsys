﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation.Controllers;

namespace LRTrafficSimulator.Simulation
{
    //This class models a robot, with its sensors and attuators
    public abstract class Robot
    {
        private Dictionary<string, ISensor> sensors;
        private Dictionary<string, IAttuator> attuators;
        private IController controller; 

        //Gets or sets the robot controller
        public IController Controller
        {
            get
            {
                return controller;
            }
            set
            {
                controller = value;
            }
        }

        //Gets the sensors' dictionary
        public Dictionary<string, ISensor> Sensors
        {
            get
            {
                return sensors;
            }
        }

        //Gets the attuators' dictionary
        public Dictionary<string, IAttuator> Attuators
        {
            get
            {
                return attuators;
            }
        }

        protected Robot(List<ISensor> sensors, List<IAttuator> attuators)
        {
            this.sensors = new Dictionary<string, ISensor>();
            this.attuators = new Dictionary<string, IAttuator>();

            foreach (ISensor sensor in sensors)
            {
                this.sensors[sensor.Name] = sensor;
            }

            foreach (IAttuator att in attuators)
            {
                this.attuators[att.Name] = att;
            }

            this.controller = new VoidController();
        }

        //Executes an update step, using the currently set controller
        public void Step()
        {
            controller.Execute(Sensors, Attuators);
        }

        public override string ToString()
        {
            return "Robot";
        }
    }
}
