﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation.Environments;

namespace LRTrafficSimulator.Simulation.Sensors
{
    public class GPSPositionSensor : Sensor<Float2>
    {
        public GPSPositionSensor(string name, Float2 initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {}

        //measures the current vehicle position
        public override void updateSensorValue(IEnvironment env, VehicleState rs, float seconds)
        {
            value = rs.Position;
        }
    }
}
