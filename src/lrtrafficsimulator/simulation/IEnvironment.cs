﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LRTrafficSimulator.Simulation
{
    public interface IEnvironment
    {
        object SampleEnvInformations(Float2 position);

        void SpreadData(Float2 pos, object data, float persistence);

        List<object> PerceiveData(Float2 pos, float range);
    }
}
