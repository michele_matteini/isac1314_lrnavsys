﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;

namespace LRTrafficSimulator.Simulation
{
    public interface ISimulator
    {
        void AddRobot(Robot r, VehicleState s);

        void StartSimulation();

        void StopSimulation(bool syncWait);

        void ClearRobots();

        int RobotCount { get; }

        VehicleState GetRobotState(int index);

        List<VehicleState> GetRobotStates();

        event Action SimulationStepPerformed;

        event Action Stopped;

        Robot GetRobotAt(int index);

        bool IsRunning { get; }

        bool Stopping { get; }


    }
}
