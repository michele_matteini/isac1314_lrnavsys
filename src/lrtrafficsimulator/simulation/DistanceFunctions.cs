﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;

namespace LRTrafficSimulator.Simulation
{
    public static class DistanceFunctions
        //This class contains some distance functions used to compute the distances between 2 points. 
        //The distance is very different depending on the distance function used
        //so that the traffic gradient between vehicles will spread in a different way.
    {
        public static DistanceFunction[] AllFunctions
        {
            get
            {
                return new DistanceFunction[] { UnitDistance, LinearDistance2R, LinearDistance4R, LinearDistance8R, ExpDistance00080, ExpDistance00020, ExpDistance00010, ExpDistance00005 };
            }

        }

        public static float UnitDistance(Float2 p1, Float2 p2)
        {
            return 1f;
        }

        public static float LinearDistance2R(Float2 p1, Float2 p2)
        {
            return Math.Max(1f - (p1 - p2).Length / (SimSettings.TRAFFIC_INFO_RANGE * 2f), 0);
        }

        public static float LinearDistance4R(Float2 p1, Float2 p2)
        {
            return Math.Max(1f - (p1 - p2).Length / (SimSettings.TRAFFIC_INFO_RANGE * 4f), 0);
        }

        public static float LinearDistance8R(Float2 p1, Float2 p2)
        {
            return Math.Max(1f - (p1 - p2).Length / (SimSettings.TRAFFIC_INFO_RANGE * 8f), 0);
        }

        public static float ExpDistance00080(Float2 p1, Float2 p2)
        {
            return (float)Math.Exp(-(1f - (p1 - p2).Length) * 0.008f);
        }

        public static float ExpDistance00020(Float2 p1, Float2 p2)
        {
            return (float)Math.Exp(-(1f - (p1 - p2).Length) * 0.002f);
        }

        public static float ExpDistance00010(Float2 p1, Float2 p2)
        {
            return (float)Math.Exp(-(1f - (p1 - p2).Length) * 0.001f);
        }

        public static float ExpDistance00005(Float2 p1, Float2 p2)
        {
            return (float)Math.Exp(-(1f - (p1 - p2).Length) * 0.0005f);
        }
    }

    public delegate float DistanceFunction(Float2 p1, Float2 p2);

}
