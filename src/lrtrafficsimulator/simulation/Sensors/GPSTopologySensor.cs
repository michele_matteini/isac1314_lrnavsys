﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation.Environments;

namespace LRTrafficSimulator.Simulation.Sensors
{
    public class GPSTopologySensor : Sensor<IntersectionInfo>
    {

        public GPSTopologySensor(string name, IntersectionInfo initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {}

        //perceives the environmental informations in the current vehicle position
        public override void updateSensorValue(IEnvironment env, VehicleState rs, float seconds)
        {
            //Transformed sensor position
            float sampleDeg = rs.Direction.Degrees + this.Position.Degrees;
            Float2 rotSensorOffset = new Float2(sampleDeg) * this.Position.Length;
            Float2 GPSSensorPos = rotSensorOffset + rs.Position;

            CityTile tile = (CityTile)env.SampleEnvInformations(GPSSensorPos);

            value = tile.Info;
        }

    }
}
