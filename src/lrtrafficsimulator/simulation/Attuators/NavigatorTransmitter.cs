﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Attuators
{
    public class NavigatorTransmitter : Attuator<float>
    {
        private float lastSignalTime;
        private const float DEST_REACHED = 0.01f;

        public NavigatorTransmitter(string name, float initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {
            lastSignalTime = (float)SimGlobal.Random.NextDouble() * SimSettings.NAV_PACKET_DT;
        }

        //transmit the traffic gradient values every NAV_PACKET_DT seconds
        public override void commitAttuatorChanges(IEnvironment env, ref VehicleState rs, float seconds)
        {
            lastSignalTime -= seconds;
            if (lastSignalTime <= 0 && (rs.FinalPosition - rs.Position).Length > DEST_REACHED)
            {
                env.SpreadData(rs.Position, new NavPacket(rs.Position, value), SimSettings.NAV_PACKET_PERSISTENCE);
                lastSignalTime = SimSettings.NAV_PACKET_DT;
            }      
        }
    }

    public struct NavPacket
    {
        public Float2 Pos;
        public float GradientValue;

        public NavPacket(Float2 pos, float gradientValue)
        {
            Pos = pos;
            GradientValue = gradientValue;
        }
    }

}
