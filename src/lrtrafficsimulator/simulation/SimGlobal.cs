﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;
using System.Threading;

namespace LRTrafficSimulator.Simulation
{
    public static class SimGlobal
    {
        //Creates and returns a singleton of a Random
        private static Random rnd;
        public static Random Random
        {
            get
            {
                if (rnd == null)
                {
                    rnd = new Random();
                }

                return rnd;
            }
        }

        //Sets the seed for the simulation in order to make it reproducible
        public static void SetRandomSeed(int seed)
        {
            rnd = new Random(seed);
        }

        private static GlobalTime globalTime;
        //Creates and returns a singleton of a GlobalTime
        public static GlobalTime Time
        {
            get
            {
                if (globalTime == null)
                {
                    globalTime = new GlobalTime();
                }

                return globalTime;
            }
        }

        public static int Min(int x1, int x2, int x3)
        {
            return x1 < x2 ? (x1 < x3 ? x1 : x3) : (x2 < x3 ? x2 : x3);
        }

        //Draws an Image <img> rotated by <rotDegrees>
        public static void DrawRotatedImg(Graphics g, Image img, float rotDegrees, int x, int y, int width, int height)
        {
            g.TranslateTransform(x, y);
            g.RotateTransform(90 - rotDegrees);
            g.TranslateTransform(-x, -y);
            g.DrawImage(img, x - width / 2, y - height / 2, width, height);
            g.ResetTransform();
        }

        public static DistanceFunction DistanceFunction { get; set; }

        public static ChoiceFunction ChoiceFunction { get; set; }

        public static int CurrentRobot { get; set; }

        private static Semaphore drawSemaphore;
        //Creates and returns a singleton of a Semaphore
        public static Semaphore DrawSemaphore
        {
            get
            {
                if (drawSemaphore == null)
                {
                    drawSemaphore = new Semaphore(1, 1);
                }

                return drawSemaphore;
            }
        }


    }
}
