﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCSwarmSimulator.BaseTypes
{
    public static class FuzzyFloatExtension
    {
        public static float Saturate(this float value)
        {
            return value > 1 ? 1 : (value < 0 ? 0 : value);
        }

        public static float Step(this float value, float thr)
        {
            return value > thr ? 1 : 0;
        }

    }
}
