﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation.Environments;

namespace LRTrafficSimulator.Simulation.Sensors
{
    public class QueuedSensor : Sensor<bool>
    {
        private const float RANGE = 10f;

        public QueuedSensor(string name, bool initialValue, Float2 pos)
            : base(name, initialValue, pos)
        {}

        //perceives if the vehicle has other cars in front of itself
        public override void updateSensorValue(IEnvironment env, VehicleState rs, float seconds)
        {
            //Transformed sensor position
            float sampleDeg = rs.Direction.Degrees + this.Position.Degrees;
            Float2 sampleNormal = new Float2(sampleDeg);
            Float2 rotSensorOffset = sampleNormal * this.Position.Length;
            Float2 queuedSensorPos = rotSensorOffset + rs.Position;

            Float2 offset = sampleNormal * RANGE;

            CityTile tile = (CityTile)env.SampleEnvInformations(queuedSensorPos + offset);
            value = tile.QueuedCars && Math.Sign(Float2.DotProduct(tile.AheadCarDir,rs.Direction)) > -0.2;
            tile = (CityTile)env.SampleEnvInformations(queuedSensorPos);
            value = tile.QueuedCars && Math.Sign(Float2.DotProduct(tile.AheadCarDir, rs.Direction)) > -0.2;
        }

    }
}
