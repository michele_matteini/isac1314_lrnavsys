﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.BaseTypes;

namespace LRTrafficSimulator.Simulation.Sensors
{
    public class GPSDirectionSensor : Sensor<Float2>
    {
        public GPSDirectionSensor(string name, Float2 initialValue, Float2 pos)
            : base(name, initialValue, pos)
        { }

        //measures the current vehicle direction
        public override void updateSensorValue(IEnvironment env, VehicleState rs, float seconds)
        {
            value = rs.Direction;
        }
    }
}
