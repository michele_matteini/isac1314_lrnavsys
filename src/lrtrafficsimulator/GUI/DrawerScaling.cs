﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.GUI
{
    //This class manages all the scaling operations
    public class DrawerScaling
    {
        private static DrawerScaling instance;
        private RectangleF visibleArea;
        private Size controlSize;

        private DrawerScaling()
        {
        }

        //Creates and/or returns a singleton for this class
        public static DrawerScaling Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DrawerScaling();
                }

                return instance;
            }
        }

        //Returns a RectangleF representing the visible area
        public RectangleF VisibleArea
        {
            get
            {
                return visibleArea;
            }
            set
            {
                visibleArea = value;
            }
        }

        //Returns the Size of the control where the gui is visualized
        public Size TargetControlSize
        {
            get
            {
                return controlSize;
            }
            set
            {
                controlSize = value;
            }
        }

        //Converts the pixel (x,y) to the real coordinates <coords>
        public void PixelsToRealCoords(int x, int y, out Float2 coords)
        {
            y = controlScaleInPixel() - y;
            coords.X = (float)x * RealPixelSize() + visibleArea.X;
            coords.Y = (float)y * RealPixelSize() + visibleArea.Y;
        }     

        //Converts the real coordinates <coords> to the pixel (x,y)
        public void RealCoordsToPixels(Float2 coords, out int x, out int y)
        {
            coords.X = (coords.X - visibleArea.X);
            coords.Y = (coords.Y - visibleArea.Y);

            x = (int)(coords.X / RealPixelSize());
            y = controlScaleInPixel() - (int)(coords.Y / RealPixelSize());
        }

        //Converts from pixel size to real coordinates system size
        public float PixelsToRealSize(int pixels)
        {
            return (float)pixels * RealPixelSize();
        }

        //Converts from real coordinates system size to pixel size
        public float RealSizeToPixels(float realSize)
        {
            return realSize / RealPixelSize();
        }

        //Gets the size of a pixel in the real coordinates system
        public float RealPixelSize()
        {
            float drawRatio = (float)controlSize.Width / (float)controlSize.Height;
            float viewRatio = visibleArea.Width / visibleArea.Height;
            if(viewRatio > drawRatio)
            {
                return visibleArea.Width / (float)controlSize.Width;
            }
            else
            {
                return visibleArea.Height / (float)controlSize.Height;
            }
        }

        //Gets the size in pixel used as reference for scaling between coord. systems.
        //The real area will be scaled according to the smallest-fitting dimension of the control to avoid unused margins.
        private int controlScaleInPixel()
        {
            float drawRatio = (float)controlSize.Width / (float)controlSize.Height;
            float viewRatio = visibleArea.Width / visibleArea.Height;
            if (viewRatio > drawRatio)
            {
                return controlSize.Width;
            }
            else
            {
                return controlSize.Height;
            }
        }

    }

}
