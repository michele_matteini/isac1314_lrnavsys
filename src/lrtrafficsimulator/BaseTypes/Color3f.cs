﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.BaseTypes
{
    //This class models a color with RGB components
    public struct Color3f
    {
        public float R, G, B;

        //Creates a Color3f from RGB components
        public Color3f(float r, float g, float b)
        {
            this.R = r;
            this.G = g;
            this.B = b;
        }
        
        //Gets a similarity value between 0 (completely different) and 1 (equal), which indicates how much this Color3f is similar to the <other>
        public float SimilarTo(Color3f other)
        {
            Color3f c1 = this.Normalize;
            Color3f c2 = other.Normalize;
            float dr = c1.R - c2.R, dg = c1.G - c2.G, db = c1.B - c2.B;
            return 1f - (float)Math.Sqrt(dr * dr + dg * dg + db * db);
        }
        
        //Returns a new Color3f obtained doing the sum between <c1> and <c2>
        public static Color3f operator +(Color3f c1, Color3f c2)
        {
            return new Color3f(c1.R + c2.R, c1.G + c2.G, c1.B + c2.B);
        }

        //Returns a new Color3f obtained dividing <c1> by the constant value <k>
        public static Color3f operator /(Color3f c1, float k)
        {
            return new Color3f(c1.R / k, c1.G / k, c1.B / k);
        }

        //Returns a new Color3f obtained multiplying <c1> and the constant value <k>
        public static Color3f operator *(Color3f c1, float k)
        {
            return new Color3f(c1.R * k, c1.G * k, c1.B * k);
        }

        //Returns a new Color3f obtained multiplying <c1> and <c2>
        public static Color3f operator *(Color3f c1, Color3f c2)
        {
            return new Color3f(c1.R * c2.R, c1.G * c2.G, c1.B * c2.B);
        }

        //Returns true if <c1> and <c2> are equals
        public static bool operator ==(Color3f c1, Color3f c2)
        {
            return c1.R == c2.R && c1.G == c2.G && c1.B == c2.B;
        }

        //Returns true if <c1> and <c2> are not equals
        public static bool operator !=(Color3f c1, Color3f c2)
        {
            return c1.R != c2.R || c1.G != c2.G || c1.B != c2.B;
        }

        //Returns true if this Color3f is equal to <obj>
        public override bool Equals(object obj)
        {
            return this == (Color3f)obj;
        }

        //Gets a hash code of Color3f
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        private const float wr = 0.299f, wg = 0.587f, wb = 0.114f;
        //Gets the luminance of this Color3f
        public float Luminance
        {
            get
            {
                return wr * R + wg * G + wb * B;
            }
        }

        //Gets the length of this Color3f
        public float Length
        {
            get
            {
                return (float)Math.Sqrt(R * R + G * G * B * B);
            }
        }

        //Gets a new Color3f that is the normalized version of this Color3f
        private const double minL = 0.00001;
        public Color3f Normalize
        {
            get
            {
                double l = Math.Sqrt(R * R + G * G + B * B);
                double unitGrey = Math.Sqrt(1.0 / 3.0);
                if (l > minL)
                {
                    return new Color3f((float)(R / l), (float)(G / l), (float)(B / l));
                }
                else if (l > float.Epsilon && l < minL)
                {
                    Color3f normColor = new Color3f((float)(R / l), (float)(G / l), (float)(B / l)) * (float)(l / minL);
                    unitGrey *= (1.0 - l / minL);
                    return new Color3f(normColor.R + (float)unitGrey, normColor.G + (float)unitGrey, normColor.B + (float)unitGrey).Normalize;
                }
                else
                {
                    return new Color3f((float)unitGrey, (float)unitGrey, (float)unitGrey);
                }
            }
        }

        //Converts this Color3f to Color
        public Color ToGuiColor()
        {
            return Color.FromArgb((int)Math.Min(R * 255f + 0.5f, 255), (int)Math.Min(G * 255f + 0.5f, 255), (int)Math.Min(B * 255f + 0.5f, 255));
        }
    }
}
