﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.BaseTypes
{
    //This class models a set of points
    public class PositionSet
    {
        private List<Float2> points;
        private float minx, maxx, miny, maxy;
        private bool indexed;

        //pos buckets
        private Float2[] pbucketPos;
        private float[] pbucketRange;
        private List<List<Float2>> pbuckets;
        private List<List<int>> pbucketsIndices;

        //Creates a new PositionSet
        public PositionSet()
        {
            points = new List<Float2>();
        }

        //Gets the number of points in this PositionSet
        public int Count
        {
            get
            {
                return points.Count;
            }
        }

        //Gets or sets the point at index <i>
        public Float2 this[int i]
        {
            get
            {
                return points[i];
            }
            set
            {
                points.RemoveAt(i);
                AddPoint(value);
            }
        }

        //Adds a new point (x,y) to this PositionSet
        public void AddPoint(float x, float y)
        {
            AddPoint(new Float2(x, y));
        }

        //Adds a new Float2 point to this PositionSet
        public void AddPoint(Float2 point)
        {
            points.Add(point);

            if (points.Count > 0)
            {
                minx = point.X < minx ? point.X : minx;
                miny = point.Y < miny ? point.Y : miny;
                maxx = point.X > maxx ? point.X : maxx;
                maxy = point.Y > maxy ? point.Y : maxy;
            }
            else
            {
                minx = point.X;
                miny = point.Y;
                maxx = point.X;
                maxy = point.Y;
            }

            indexed = false;
        }

        //Spread the points in this PositionSet into different buckets in order to access them in a more efficient way
        public void ComputeIndex()
        {
            int bucketCount = (int)Math.Sqrt(points.Count);
            int nbucket = (int)Math.Sqrt(bucketCount);
            bucketCount = nbucket * nbucket;


            pbucketPos = new Float2[bucketCount];
            pbucketRange = new float[bucketCount];
            pbuckets = new List<List<Float2>>();
            pbucketsIndices = new List<List<int>>();


            //generate bucket positions
            float xstep = (maxx - minx) / (nbucket - 1);
            float ystep = (maxy - miny) / (nbucket - 1);
            for (int x = 0; x < nbucket; x++)
            {
                for (int y = 0; y < nbucket; y++)
                {
                    int i = x + y * nbucket;
                    pbucketPos[i] = new Float2(x * xstep, y * ystep);
                    pbuckets.Add(new List<Float2>());
                    pbucketsIndices.Add(new List<int>());
                }
            }

            //fill buckets
            int nearestBucket;
            float distance;

            for (int i = 0; i < points.Count; i++)//for each node
            {
                nearestBucket = 0;
                distance = (points[i] - pbucketPos[0]).Length;
                for (int bki = 1; bki < bucketCount; bki++)//search the nearest bucket
                {
                    float d = (points[i] - pbucketPos[bki]).Length;
                    if (d < distance)
                    {
                        distance = d;
                        nearestBucket = bki;
                    }
                }

                pbuckets[nearestBucket].Add(points[i]);
                pbucketsIndices[nearestBucket].Add(i);
                if (pbucketRange[nearestBucket] < distance)
                {
                    pbucketRange[nearestBucket] = distance;
                }
            }

            indexed = true;
        }

        //Gets the index and the distance <ndist> from the position <pos> to the closest point in this PositionSet
        public int GetClosestNode(Float2 pos, out float ndist)
        {
            if (!indexed)
            {
                ComputeIndex();
            }

            int nearestBucket = -1;
            ndist = 0;
            float distance = 0;

            //compute the nearest bucket
            for (int i = 0; i < pbucketPos.Length; i++)
            {
                float d = (pbucketPos[i] - pos).Length;
                if ((nearestBucket < 0 || distance > d) && pbuckets[i].Count > 0)
                {
                    nearestBucket = i;
                    distance = d;
                }
            }

            if (nearestBucket < 0) return -1;//nessun nodo

            //search in the nearest bucket
            int nearInBucket = 0;
            ndist = (pbuckets[nearestBucket][0] - pos).Length;
            for (int i = 1; i < pbuckets[nearestBucket].Count; i++)
            {
                float di = (pbuckets[nearestBucket][i] - pos).Length;
                if (ndist > di)
                {
                    ndist = di;
                    nearInBucket = i;
                }
            }

            //search in the other buckets
            int finalBucket = nearestBucket;
            for (int i = 0; i < pbucketPos.Length; i++)
            {
                if (i != nearestBucket && ((pbucketPos[i] - pos).Length - pbucketRange[i]) < ndist)//skip buckets too far away
                {
                    for (int bi = 0; bi < pbuckets[i].Count; bi++)
                    {
                        float di = (pbuckets[i][bi] - pos).Length;
                        if (ndist > di)
                        {
                            ndist = di;
                            nearInBucket = bi;
                            finalBucket = i;
                        }
                    }
                }
            }

            return pbucketsIndices[finalBucket][nearInBucket];
        }

        //Returns the list of the indices of all points in this PositionSet having a distance smaller or equal to <range> from the position <pos>
        public List<int> GetNodesInRange(Float2 pos, float range)
        {
            List<int> result = new List<int>();

            if (!indexed)
            {
                ComputeIndex();
            }

            //search in buckets
            for (int bi = 0; bi < pbucketPos.Length; bi++)
            {
                if (((pbucketPos[bi] - pos).Length - pbucketRange[bi]) <= range)
                {
                    //search nodes in range in bucket [bi]
                    for (int i = 0; i < pbuckets[bi].Count; i++)
                    {
                        float di = (pbuckets[bi][i] - pos).Length;
                        if (di <= range)
                        {
                            result.Add(pbucketsIndices[bi][i]);
                        }
                    }
                }
            }

            return result;
        }

    }
}
