﻿using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.GUI;
using LRTrafficSimulator.Simulation.Robots;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation.Environments
{
    public partial class CityMapWithVehicles : CityMap
        //This class models some vehicles within a city topology 
    {
        private List<VehicleState> robotStates;
        private IBoundingSurface vShape;
        private int selRobotIndex;
        private Navigator nav;

        //Creates a new CityMapWithVehicles with a certain <radius> and a certain number of intersections (<nodeCount>)
        public CityMapWithVehicles(float radius, int nodeCount)
            : base(radius, nodeCount)
        {
            initialize();
        }

        //Creates a new CityMapWithVehicles from a file <path>
        public CityMapWithVehicles(string path)
            : base(path)
        {
            initialize();
        }

        private void initialize()
        {
            this.vShape = new VehicleShape(Vehicle.VEHICLE_WIDTH, Vehicle.VEHICLE_LENGTH);
            this.robotStates = new List<VehicleState>();
            selRobotIndex = -1;
            nav = new Navigator(this);
        }

        public void SetRobotStates(List<VehicleState> states)
        {
            this.robotStates = new List<VehicleState>(states);
        }

        //Returns the environment informations in the particular <position>
        protected override object sampleEnvInformations(Float2 position)
        {
            CityTile tile = (CityTile)base.sampleEnvInformations(position);
            float vsquaredWidth = Vehicle.VEHICLE_WIDTH * Vehicle.VEHICLE_WIDTH;

            for (int i = 0; i < robotStates.Count; i++)
            {
                if (i == SimGlobal.CurrentRobot) continue;

                VehicleState s = robotStates[i];
                if (Float2.SquaredDistance(s.Position, position) < vsquaredWidth)
                {
                    if (vShape.DistanceFrom(position, s.Position, s.Direction) == 0)
                    {
                        tile.AheadCarDir = s.Direction;
                        tile.QueuedCars = true;
                        break;
                    }
                }
            }

            return tile;
        }

        public void SetSelectedRobot(int index)
        {
            selRobotIndex = index;
        }

        //Returns the index of the closest vehicle to the position <coords> and its <distance>
        public int GetClosestRobot(Float2 coords, out float distance)
        {
            distance = 0;
            if (robotStates.Count == 0) return -1;

            int closestVehicle = 0;
            distance = (robotStates[0].Position - coords).Length;

            for (int i = 1; i < robotStates.Count; i++)
            {
                float di = (robotStates[i].Position - coords).Length;
                if (di < distance)
                {
                    closestVehicle = i;
                    distance = di;
                }
            }
            return closestVehicle;
        }

        //Computes an initial random position and a destination to assign to a vehicle
        public VehicleState GetRndRobotPlacement()
        {
            VehicleState vs = new VehicleState();

            //random vehicle positioning
            int nextNode = 0, i;
            while (true)
            {
                nextNode = SimGlobal.Random.Next(0, nodes.Count);
                for (i = 0; i < nodes.Count && !street[nextNode, i]; i++) ;

                if (i != nodes.Count)
                {
                    float rlen = (nodes[nextNode] - nodes[i]).Length;
                    if (rlen > SimSettings.SEMAPHORE_RANGE * 2) break;
                }
            }

            float sp = (float)SimGlobal.Random.NextDouble() * 0.5f + 0.25f;
            Float2 nextNodePos = nodes[nextNode];
            Float2 pos2 = nodes[i];
            vs.Position = nextNodePos * sp + pos2 * (1 - sp);
            vs.Direction = (nextNodePos - pos2).GetNormal();
            
            //random destination vehicle path
            int destNode = destinations.ElementAt(SimGlobal.Random.Next(DestinationCount));
            Float2[] path = nav.ComputeGPSPath(i, nextNode, destNode);
            vs.Path = path;
            vs.NextNode = 1;

            return vs;
        }
    }

}
