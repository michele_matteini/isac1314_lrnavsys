﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.Simulation.Attuators;
using LRTrafficSimulator.Simulation.Environments;

namespace LRTrafficSimulator.Simulation.Sensors
{
    public class TrafficSensor : Sensor<TrafficInfo>
    {
        public TrafficSensor(string name, TrafficInfo initialValue, Float2 pos)
            : base(name, initialValue, pos)
        { }

        //perceives the traffic gradient values in a TRAFFIC_INFO_RANGE, weighting them by the distance function selected from the gui
        //in order to compute the traffic gradient value for this vehicle
        public override void updateSensorValue(IEnvironment env, VehicleState rs, float seconds)
        {
            Float2 curPos = rs.Position;
            List<object> info = env.PerceiveData(curPos, SimSettings.TRAFFIC_INFO_RANGE);

            TrafficInfo ti = new TrafficInfo();
            CityTile tile = (CityTile)env.SampleEnvInformations(curPos);
            Float2[] streetDirs;
            float[] streetWeight;

            if (tile.Intersection)
            {
                int length = tile.Info.Positions.Length;
                ti.DirectionGradients = new float[length];
                streetDirs = new Float2[length];
                streetWeight = new float[length];
            }
            else
            {
                ti.DirectionGradients = new float[] { };
                streetDirs = new Float2[] { };
                streetWeight = new float[] { };
            }

            for (int i = 0; i < ti.DirectionGradients.Length; i++)
            {
                streetDirs[i] = (tile.Info.Positions[i] - curPos).GetNormal();
            }

            //compute its gradient value
            float sumWeights = 0f;
            
            foreach (object o in info)
            {
                NavPacket np = (NavPacket)o;
                Float2 sampleDir = (np.Pos - curPos).GetNormal();
                float weight = SimGlobal.DistanceFunction(curPos, np.Pos);
                float weightedGradient = np.GradientValue * weight;
                
                sumWeights += 1f;
                ti.TrafficGradient += weightedGradient;

                //if there is an intersection, compute gradients in every viable direction from that point
                for (int i = 0; i < ti.DirectionGradients.Length; i++)
                {
                    float dotWeight = Float2.DotProduct(sampleDir, streetDirs[i]);
                    if (dotWeight > 0)
                    {
                        ti.DirectionGradients[i] += (weightedGradient * dotWeight);
                        streetWeight[i] += dotWeight;
                    }
                }
            }

            float localTraffic = info.Count / (SimSettings.TRAFFIC_INFO_RANGE * SimSettings.TRAFFIC_INFO_RANGE);
            ti.TrafficGradient /= sumWeights;
            ti.TrafficGradient = Math.Max(localTraffic, ti.TrafficGradient);
            for (int i = 0; i < ti.DirectionGradients.Length; i++)
            {
                if (streetWeight[i] > 0)
                {
                    ti.DirectionGradients[i] /= streetWeight[i];
                }
            }

            value = ti;
        }
    }

    public struct TrafficInfo
    {
        public float TrafficGradient;
        public float[] DirectionGradients;
    }
}
