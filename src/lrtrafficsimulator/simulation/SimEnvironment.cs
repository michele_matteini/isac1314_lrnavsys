﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRTrafficSimulator.Simulation
{
    public abstract class SimEnvironment : IEnvironment
    {
        protected float lastFrameTime;
        protected List<SpreadData> diffusedData;

        private const int CACHE_SIZE = 6;
        private int lastCacheIndex;
        private object[] sampleCache; //cache containing up to CACHE_SIZE sampled environment informations
        private int[] lastSampleHash;

        //Creates a new SimEnvironment
        protected SimEnvironment()
        {
            diffusedData = new List<SpreadData>();
            sampleCache = new object[CACHE_SIZE];
            lastSampleHash = new int[CACHE_SIZE];
        }

        //Returns the environment informations in the particular <position>
        public object SampleEnvInformations(Float2 position)
        {
            if (SimGlobal.Time.SimTimeSeconds != lastFrameTime)
            {
                lastFrameTime = SimGlobal.Time.SimTimeSeconds;
                ClearCache();
            }

            int sampleHash = position.GetHashCode();

            int i;
            for (i = 0; i < CACHE_SIZE && lastSampleHash[i] != sampleHash; i++) ;
            object curSample = null;
            if (i != CACHE_SIZE) curSample = sampleCache[i];

            if (curSample == null)
            {
                curSample = sampleEnvInformations(position);
                lastSampleHash[lastCacheIndex] = sampleHash;
                sampleCache[lastCacheIndex] = curSample;
                lastCacheIndex = (lastCacheIndex + 1) % CACHE_SIZE;
            }

            return curSample;
        }

        protected abstract object sampleEnvInformations(Float2 position);

        //Spreads the traffic gradient values in the city
        public virtual void SpreadData(Float2 pos, object data, float persistence)
        {
            SpreadData sd = new SpreadData();
            sd.data = data;
            sd.pos = pos;
            sd.expirationTime = SimGlobal.Time.SimTimeSeconds + persistence;
            diffusedData.Add(sd);
        }

        //Perceives the traffic gradient values in a <range> centered in the position <pos>
        public virtual List<object> PerceiveData(Float2 pos, float range)
        {
            List<object> objInRange = new List<object>();
            float r2 = range * range;
            foreach (SpreadData sd in diffusedData)
            {
                if (Float2.SquaredDistance(sd.pos, pos) <= r2)
                {
                    objInRange.Add(sd.data);
                }
            }
            return objInRange;
        }

        //Clears the sample cache
        public void ClearCache()
        {
            diffusedData.RemoveAll(x => x.expirationTime < SimGlobal.Time.SimTimeSeconds);
            for (int i = 0; i < CACHE_SIZE; i++)
            {
                sampleCache[lastCacheIndex] = null;
            }
        }
    }

    public struct SpreadData
    {
        public float expirationTime;
        public Float2 pos;
        public object data;
    }
}
