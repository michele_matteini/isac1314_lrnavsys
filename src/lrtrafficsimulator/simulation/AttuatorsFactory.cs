﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.Simulation.Attuators;
using LRTrafficSimulator.BaseTypes;

namespace LRTrafficSimulator.Simulation
{
    //This class handles the attuators' creation
    public class AttuatorsFactory
    {
        public AttuatorsFactory()
        {

        }

        public IAttuator CreateDoubleLinearMotor(string name, Float2 initialValue, Float2 pos, Float2 dir, float wheelsDistance)
        {
            return (IAttuator)new DoubleLinearMotor(name, initialValue, pos, dir, wheelsDistance);
        }

        public IAttuator CreateNavigationGuide(string name, bool initialValue, Float2 pos)
        {
            return (IAttuator)new NavigationGuide(name, initialValue, pos);
        }

        public IAttuator CreateParkingAttuator(string name, bool initialValue, Float2 pos)
        {
            return (IAttuator)new ParkingAttuator(name, initialValue, pos);
        }

        public IAttuator CreateNavigatorTransmitter(string name, float initialValue, Float2 pos)
        {
            return (IAttuator)new NavigatorTransmitter(name, initialValue, pos);
        }

        public IAttuator CreateNavigatorUpdater(string name, PathUpdatingInfo initialValue, Float2 pos)
        {
            return (IAttuator)new NavigatorUpdater(name, initialValue, pos);
        }

    }
}
