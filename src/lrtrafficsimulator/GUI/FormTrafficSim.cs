﻿using LRTrafficSimulator.BaseTypes;
using LRTrafficSimulator.Simulation;
using LRTrafficSimulator.Simulation.Controllers;
using LRTrafficSimulator.Simulation.Environments;
using LRTrafficSimulator.Simulation.Robots;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LRTrafficSimulator.GUI
{
    //This class handles the traffic simulator gui
    public partial class FormTrafficSim : Form
    {
        private ISimulator sim;
        private SimPanel viewPanel;
        private CityMapWithVehicles env;
        private OpenFileDialog openMapDialog;
        private List<float> vArrivalTimes;       

        //Initialize the gui
        public FormTrafficSim()
        {
            InitializeComponent();

            openMapDialog = new OpenFileDialog();

            //load controllers
            comboBoxGPSAI.Items.AddRange(ControllersList.GetControllerNames());
            comboBoxGPSAI.SelectedIndex = 0;

            //view type
            comboBoxVisual.SelectedIndex = 0;

            //load distance functions
            foreach (DistanceFunction f in DistanceFunctions.AllFunctions)
            {
                comboBoxDFuncs.Items.Add(f.Method.Name);
            }
            comboBoxDFuncs.SelectedIndex = 0;

            //load choice functions
            foreach (ChoiceFunction f in ChoiceFunctions.AllFunctions)
            {
                comboBoxCFuncs.Items.Add(f.Method.Name);
            }
            comboBoxCFuncs.SelectedIndex = 0;

            //idle event
            Application.Idle += Application_Idle;

            //list of vehicle arrival times
            vArrivalTimes = new List<float>();
        }

        //Initializes the view simulation panel
        private void initViewPanel()
        {
            DrawerScaling.Instance.TargetControlSize = splitContainer1.Panel2.ClientSize;
            viewPanel.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
            viewPanel.BackColor = System.Drawing.Color.DimGray;
            viewPanel.Location = new System.Drawing.Point(0, 0);
            viewPanel.Name = "viewPanel";
            viewPanel.Size = splitContainer1.Panel2.ClientSize;
            viewPanel.TabIndex = 0;
            viewPanel.TabStop = false;
            viewPanel.SimMouseClick += new Action<Float2>(viewPanel_SimMouseClick);
            splitContainer1.Panel2.Controls.Add(viewPanel);
        }  

        //Creates a new simulation (either random or from a map loaded from file)
        private int repetitionsLeft;
        private void newSimButton_Click(object sender, EventArgs e)
        {
            if (checkBoxMapFromFile.Checked)
            // load map from file
            {
                DialogResult result = openMapDialog.ShowDialog();
                if (result != System.Windows.Forms.DialogResult.OK) return;
            }

            if (sender != this)
            {
                if (checkBoxRepeat.Checked)
                {
                    repetitionsLeft = (int)nRepeatTimes.Value - 1;
                }
                else
                {
                    repetitionsLeft = 0;
                }
            }

            //reset time
            SimGlobal.Time.Reset();

            //reset GUI
            lblSimEndTime.Text = "N/A";
            lblSpeedMul.Text = "x1";
            lblSimEndTime.ForeColor = Color.Teal;
            lblArrivedVehicles.Text = String.Format("0/{0} (0%)", (int)nCars.Value);
            lstVehicleTimes.Items.Clear();
            vArrivalTimes.Clear();


            SimSettings.TRAFFIC_INFO_RANGE = (float)nTransmRange.Value;

            if (checkBoxSeed.Checked)
            //sets the seed in order to make the simulation reproducible
            {
                SimGlobal.SetRandomSeed(txtSeed.Text.GetHashCode());
            }

            //create environment
            int nodeCount = (int)nIntersections.Value;
            float mapDim;
            if (checkBoxMapFromFile.Checked)
                //from file
            {
                env = new CityMapWithVehicles(openMapDialog.FileName);
                mapDim = env.MapRadius;
            }
            else
                //random
            {
                mapDim = (float)(Math.Sqrt(nodeCount) * SimSettings.MAP_DIM_MUL);
                env = new CityMapWithVehicles(mapDim, nodeCount);
            }

            //initializes the visible area
            DrawerScaling.Instance.VisibleArea = new RectangleF(
                -mapDim,
                -mapDim,
                2f * mapDim,
                2f * mapDim
            );

            //create simulator
            sim = new DiscreteTimeSimulator(env);
            sim.SimulationStepPerformed += SimulationStepPerformed;
            sim.Stopped += sim_Stopped;

            //create navigator
            CityMap.Navigator nav = new CityMap.Navigator(env);

            //create robots and add them to simulator
            SensorsFactory sf = new SensorsFactory();
            AttuatorsFactory af = new AttuatorsFactory();
            if (!checkBoxMapFromFile.Checked)
            {
                env.DestinationCount = (int)nDestinations.Value;
            }
            comboBoxVehicles.Items.Clear();
            comboBoxVehicles.Items.Add("<select a vehicle>");
            Float2 parkingPos = new Float2(-mapDim - 150, -mapDim - 200);
            for (int i = 0; i < (int)nCars.Value; i++)
            {
                Robot r = new Vehicle(sf, af);
                VehicleState s = env.GetRndRobotPlacement();
                s.FinalPosition = new Float2(i * SimSettings.CARS_PARKING_PADDING, 0) + parkingPos;
                s.Nav = nav;
                sim.AddRobot(r, s);
                comboBoxVehicles.Items.Add("Vehicle #" + i);
            }

            updateRobotsController();
            env.SetRobotStates(sim.GetRobotStates());


            InitDrawer(checkBoxMapFromFile.Checked? openMapDialog.FileName : string.Empty);
            
            EnableDisableButtons("NewSim");
            comboBoxVehicles.SelectedIndex = 0;
        }

        //Starts or pauses the current simulation
        private void startPauseButton_Click(object sender, EventArgs e)
        {
            if (startPauseButton.Text.Equals("Start"))
            {
                sim.StartSimulation();
                startPauseButton.Text = "Stop";
                EnableDisableButtons("Start");
            }
            else if (startPauseButton.Text.Equals("Stop"))
            {
                sim.StopSimulation(false);
                startPauseButton.Text = "Start";
                EnableDisableButtons("Stop");
            }
        }

        //When the stopped event comes, gui will be closed
        private void sim_Stopped()
        {
            if (closeRequest)
            {
                if (InvokeRequired)
                {
                    this.BeginInvoke(new Action(sim_Stopped));
                }
                else
                {
                    this.Close();
                }
            }
        }

        int skipFrameEnabled;
        int skipFrame;
        //executes a simulation step
        private void SimulationStepPerformed()
        {
            if (this.InvokeRequired)
                //not the gui thread
            {
                env.SetRobotStates(sim.GetRobotStates());
                this.BeginInvoke(new MethodInvoker(SimulationStepPerformed));
            }
            else
                //gui thread
            {
                if (skipFrameEnabled > 0)
                {
                    skipFrame = (skipFrame + 1) % skipFrameEnabled;

                    if (skipFrame != 0)
                        //skips a frame avoiding to draw it
                    {
                        SimGlobal.DrawSemaphore.Release();
                        return;
                    }
                }

                //update time and fps informations, then refresh gui
                this.lblTime.Text = SimGlobal.Time.ToString();
                string fpsString = SimGlobal.Time.FPS.ToString();
                if (fpsString.Length > 4) fpsString = fpsString.Substring(0, 4);
                viewPanel.DisplayedInformation = "FPS : " + fpsString;
                viewPanel.RefreshSimulation();
                releaseRequest = true;
            }
        }

        private bool releaseRequest;
        //When the gui thread is not processing events, release a semaphore lock in order to
        //allow the simulator to send other simulation frames to draw
        private void Application_Idle(object sender, EventArgs e)
        {
            if (releaseRequest)
            {
                try
                {
                    SimGlobal.DrawSemaphore.Release();
                }
                catch (SemaphoreFullException) { }
                releaseRequest = false;
            }
        }

        //Initializes the drawer
        private void InitDrawer(string mapFilePath)
        {
            ISimDrawer drawer = null;
            if (mapFilePath != string.Empty)
            {
                drawer = new TrafficDrawer(sim, env, mapFilePath);
            }
            else
            {
                drawer = new TrafficDrawer(sim, env);
            }

            if (viewPanel == null)
            {
                viewPanel = new SimPanel(drawer);
                initViewPanel();
            }
            else viewPanel.Drawer = drawer;
            viewPanel.Drawer.SetView(comboBoxVisual.SelectedItem.ToString());
            viewPanel.RefreshSimulation();
        }

        //Depending on the current simulation state, this method enable or disable some gui functionalities (buttons, menu, ...)
        private void EnableDisableButtons(string buttonPressed)
        {
            switch (buttonPressed)
            {
                case "Start":
                    newSimButton.Enabled = false;
                    slowButton.Enabled = true;
                    fastButton.Enabled = true;
                    viewPanel.MouseEnabled = true;
                    viewPanel.AutoRefreshEnabled = false;
                    startSimulatioToolStripMenuItem.Enabled = false;
                    stopToolStripMenuItem.Enabled = true;
                    break;
                case "Stop":
                    fastButton.Enabled = false;
                    slowButton.Enabled = false;
                    newSimButton.Enabled = true;
                    viewPanel.MouseEnabled = true;
                    viewPanel.AutoRefreshEnabled = true;
                    startSimulatioToolStripMenuItem.Enabled = true;
                    stopToolStripMenuItem.Enabled = false;
                    break;
                case "Fast":
                    break;
                case "Slow":
                    break;
                case "NewSim":
                    startPauseButton.Enabled = true;
                    stopToolStripMenuItem.Enabled = false;
                    startSimulatioToolStripMenuItem.Enabled = true;
                    slowButton.Enabled = false;
                    fastButton.Enabled = false;
                    viewPanel.MouseEnabled = true;
                    viewPanel.AutoRefreshEnabled = true;
                    break;
            }
        }

        //Increases the simulation speed
        private void fastButton_Click(object sender, EventArgs e)
        {
            SimGlobal.Time.StepTimeSeconds = SimGlobal.Time.StepTimeSeconds * 2;
            lblSpeedMul.Text = "x" + (float.Parse(lblSpeedMul.Text.Substring(1)) * 2);
            EnableDisableButtons("Fast");
        }

        //Decreases the simulation speed
        private void slowButton_Click(object sender, EventArgs e)
        {
            SimGlobal.Time.StepTimeSeconds = SimGlobal.Time.StepTimeSeconds * 0.5f;
            lblSpeedMul.Text = "x" + (float.Parse(lblSpeedMul.Text.Substring(1)) * 0.5);
            EnableDisableButtons("Slow");
        }

        private bool closeRequest = false;
        //When the form is closing, simulation must be stopped
        private void FormSimulator_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sim != null && !closeRequest && sim.IsRunning)
            {
                sim.StopSimulation(false);
                closeRequest = true;
                e.Cancel = true;
            }
        }

        //Assignes the selected controller to each robot 
        private void updateRobotsController()
        {
            if (comboBoxGPSAI.SelectedIndex < 0 || sim == null) return;

            string controllerName = (string)comboBoxGPSAI.SelectedItem;
            for (int i = 0; i < sim.RobotCount; i++)
            {
                IController ctrl = ControllersList.GetController(controllerName);
                ctrl.ControlEvent += ControlEvent;
                sim.GetRobotAt(i).Controller = ctrl;
            }
        }

        //When a ControlEvent comes, it means that a vehicle is arrived at its destination
        private void ControlEvent(string eventDescr)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string>(ControlEvent), eventDescr);
            }
            else
            {
                switch (eventDescr)
                {
                    case "Destination Reached":
                        //updates the simulation results
                        vArrivalTimes.Add(SimGlobal.Time.SimTimeSeconds);
                        lstVehicleTimes.Items.Add("Vehicle arrived. " + SimGlobal.Time.ToString());

                        int nVehicles = (int)nCars.Value;
                        int arrivedVehicles = vArrivalTimes.Count;
                        int arrivedVehiclePerc = (int)((float)arrivedVehicles / (float)nVehicles * 100f);
                        int arrivedVehicleThr = (int)nComplPerc.Value;

                        lblArrivedVehicles.Text = string.Format("{0}/{1} arrived. ({2}%)", arrivedVehicles, nVehicles, arrivedVehiclePerc);

                        if (arrivedVehiclePerc >= arrivedVehicleThr && lblSimEndTime.Text == "N/A")
                        {
                            lblSimEndTime.Text = SimGlobal.Time.ToString();
                            lblSimEndTime.ForeColor = Color.FromArgb(0, 220, 0);
                            lstSimEndTimes.Items.Add(SimGlobal.Time.SimTimeSeconds);

                            if (repetitionsLeft > 0)//repeat simulation
                            {
                                checkBoxSeed.Checked = false;
                                repetitionsLeft--;
                                startPauseButton_Click(this, null);
                                //nCars.Value = nCars.Value + 16;
                                newSimButton_Click(this, null);
                                startPauseButton_Click(this, null);
                                fastButton_Click(this, null);
                            }

                        }

                        break;
                }
            }
        }



        private void comboBoxControllers_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateRobotsController();
        }

        private void comboBoxVehicles_SelectedIndexChanged(object sender, EventArgs e)
        {
            env.SetSelectedRobot(comboBoxVehicles.SelectedIndex - 1);
            viewPanel.RefreshSimulation();
        }

        private void comboBoxDFuncs_SelectedIndexChanged(object sender, EventArgs e)
        {
            SimGlobal.DistanceFunction = DistanceFunctions.AllFunctions[comboBoxDFuncs.SelectedIndex];
        }

        private void comboBoxCFuncs_SelectedIndexChanged(object sender, EventArgs e)
        {
            SimGlobal.ChoiceFunction = ChoiceFunctions.AllFunctions[comboBoxCFuncs.SelectedIndex];
        }

        private const float VEHICLE_SEL_MAX_DISTANCE = 12f;
        //if a user clicks on a vehicle, it must be selected
        void viewPanel_SimMouseClick(Float2 coords)
        {
            float distance;
            int closestVehicle = env.GetClosestRobot(coords, out distance);

            if (closestVehicle < 0) return;

            if(distance < VEHICLE_SEL_MAX_DISTANCE)
            {
                comboBoxVehicles.SelectedIndex = closestVehicle + 1;
            }
        }

        private void comboBoxVisual_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (viewPanel != null)
            {
                viewPanel.Drawer.SetView(comboBoxVisual.SelectedItem.ToString());
                viewPanel.RefreshSimulation();
            }
        }

        private void startSimulatioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            startPauseButton_Click(sender, e);
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            startPauseButton_Click(sender, e);
        }

        private void comboBoxSkipFrame_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.skipFrameEnabled = comboBoxSkipFrame.SelectedIndex;
        }

        //When the clipboard button is clicked, the arrival times of vehicles will be copied to the clipboard
        private void btnCopyResults_Click(object sender, EventArgs e)
        {
            if (vArrivalTimes.Count == 0) return;

            StringBuilder sb = new StringBuilder();
            sb.Append(vArrivalTimes[0]);
            for (int i = 1; i < vArrivalTimes.Count; i++)
            {
                sb.Append(";");
                sb.Append(vArrivalTimes[i]);
            }

            Clipboard.SetText(sb.ToString());
        }

        private void btnResetSimResults_Click(object sender, EventArgs e)
        {
            lstSimEndTimes.Items.Clear();
        }

        private void btnCopySimTimes_Click(object sender, EventArgs e)
        {
            if (lstSimEndTimes.Items.Count == 0) return;

            StringBuilder sb = new StringBuilder();
            sb.Append(lstSimEndTimes.Items[0]);
            for (int i = 1; i < lstSimEndTimes.Items.Count; i++)
            {
                sb.Append(";");
                sb.Append(lstSimEndTimes.Items[i]);
            }

            Clipboard.SetText(sb.ToString());
        }

     

    }
}
