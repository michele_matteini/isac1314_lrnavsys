﻿using LRTrafficSimulator.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LRTrafficSimulator.Simulation.Sensors;
using LRTrafficSimulator.Simulation.Environments;

namespace LRTrafficSimulator.Simulation
{
    //This class handles the sensors' creation
    public class SensorsFactory
    {

        public SensorsFactory()
        {
        }

        public ISensor CreateClockSensor(string name, float initialValue, Float2 pos)
        {
            return (ISensor)new ClockSensor(name, initialValue, pos);
        }

        public ISensor CreateGPSTopologySensor(string name, IntersectionInfo initialValue, Float2 pos)
        {
            return (ISensor)new GPSTopologySensor(name, initialValue, pos);
        }

        public ISensor CreateQueuedSensor(string name, bool initialValue, Float2 pos)
        {
            return (ISensor)new QueuedSensor(name, initialValue, pos);
        }

        public ISensor CreateIntSensor(string name, bool initialValue, Float2 pos)
        {
            return (ISensor)new IntSensor(name, initialValue, pos);
        }

        public ISensor CreateGPSPositionSensor(string name, Float2 initialValue, Float2 pos)
        {
            return (ISensor)new GPSPositionSensor(name, initialValue, pos);
        }

        public ISensor CreateGPSDirectionSensor(string name, Float2 initialValue, Float2 pos)
        {
            return (ISensor)new GPSDirectionSensor(name, initialValue, pos);
        }


        public ISensor CreateRightOfWaySensor(string name, bool initialValue, Float2 pos)
        {
            return (ISensor)new RightOfWaySensor(name, initialValue, pos);
        }

        public ISensor CreatePathSensor(string name, Float2[] initialValue, Float2 pos)
        {
            return (ISensor)new PathSensor(name, initialValue, pos);
        }

        public ISensor CreateDestReachedSensor(string name, bool initialValue, Float2 pos)
        {
            return (ISensor)new DestReachedSensor(name, initialValue, pos);
        }

        public ISensor CreateTrafficSensor(string name, TrafficInfo initialValue, Float2 pos)
        {
            return (ISensor)new TrafficSensor(name, initialValue, pos);
        }
    }
}
